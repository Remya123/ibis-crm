var OfficeSearch = function(){
  
  $('#office').autocomplete({
    serviceUrl: '/employee/findoffice/',
    onSelect: function (suggestion) {
        // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    },
    paramName:'office',
    transformResult: function(response) {
        console.log(response);
        response = JSON.parse(response);
        return {
            suggestions: $.map(response, function(dataItem) {
                console.log(dataItem)
                return { value: dataItem.name, data: dataItem.id };
            })
        };
    }
  })
}
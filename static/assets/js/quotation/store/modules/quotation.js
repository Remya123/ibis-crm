import quotationApi from 'quotation/api.js'
import * as types from '../mutation-types'
// import isEmpty from '../../contrib/utils'
// import Vue from 'vue'

const state = {
	quotations: [],
	quotation: {
		id: null,
		code:null,
		status: null,
		customer: '',
		reason_for_lost: '',
		amount: 0.00,  
	},
	pagination:{
		count: 0,
		current: 1,
		page_size: 3,
		link_range: 3,
	},
	choices: {
		statuses: [],
		default_status: '',
		customers: [],
	},
	
	saving: false,
	quotation_list: true,
	edit_form: false,
	view_quotation: false,
}

const getters = {
	allQuotations: state => state.quotations,
	quotation: state => state.quotation,
	choices: state => state.choices,
	pagination: state => state.pagination,
}

const actions = {
	getQuotations ({ commit, rootState }) {
		// let url = 
		console.log('/api'+rootState.route.path)
		commit('loading')
		return new Promise((resolve) => {
			quotationApi.getQuotations('/api'+rootState.route.fullPath)
				.then( response => {
					commit(types.SET_PAGINATION,{
						count: response.data.count,
						page: rootState.route.query.page,
						page_size: response.data.page_size
					})
					commit(types.SET_QUOTATIONS, {quotations: response.data.results })
					commit('loaded')
					resolve()
				})
				.catch( error => {
					commit('loaded')
					console.log(error)
				})
		})
	},

	getQuotationOptions ({ commit }) { 
		return new Promise((resolve) => {
			commit('loading')
			quotationApi.getQuotationOptions()
				.then( response => {
					commit(types.SET_QUOTATION_OPTIONS, response.data )
					commit('loaded')
					resolve()
				})
				.catch(error => {
					commit('loaded')
					console.log(error)
				})
		})
	},

	// getEnquiry ({ commit, state }, id = null) {
	// 	if(id){
	// 		commit('loading')
	// 		quotationApi.get(id)
	// 			.then((response) => {
	// 				commit(types.SET_ENQUIRY, { enquiry: response.data })
	// 				commit('loaded')
	// 			})
	// 			.catch( error => console.log(error) )
	// 	}
	// },

	// deleteEnquiry( {commit, state}, id = null) {
	// 	if(id){
	// 		commit('loading')
	// 		quotationApi.delete('/api/enquiries/' + id + '/')
	// 			.then((response) => {
	// 				console.log(response)
	// 				commit(types.DELETE_ENQUIRY, { id })
	// 				commit('loaded')
	// 				return Promise(resolve => resolve())
	// 			})
	// 			.catch((error) => {
	// 				commit('loaded')
	// 				commit('API_FAILURE', error)
	// 			})
	// 	}
	// },

	postQuotation ({ commit, state }, quotation) {
		console.log(quotation)
		return new Promise(( resolve, reject) => {
			commit('loading')
			quotationApi.post('/api/quotations/create/', quotation)
				.then( response => {
					commit(types.SET_QUOTATION, response.data)
					// commit(types.INSERT_ENQUIRY, response.data)
					commit('loaded')

					resolve()
				})
				.catch( error => {
					commit('loaded')
					reject(error) 
				})
		})
	},

	// addItemToEnquiryItem ({commit}, payload) {
	//   // commit(types.UPDATE_ITEM_FIELD, payload )
	//   // commit(types.UPDATE_ENQUIRY_AMOUNT)

	// }

}

const mutations = {
	

	[types.SET_QUOTATIONS] (state, { quotations }) {
		state.quotations = quotations
	},
	[types.SET_QUOTATION] (state, { quotation }) {
		state.quotation = quotation
	},
	[types.INSERT_QUOTATION] (state, { quotation, created }) {
		if(created)
			state.quotations = quotation.concat(state.enquiries)
		//state.quotations.push(quotation)
	},

	[types.SET_QUOTATION_OPTIONS] (state, {statuses= [],default_status= ''}) {
		state.choices.statuses = statuses
		state.choices.default_status = default_status
		
	},

	
	[types.SET_QUOTATION_OPTION_DEFAULTS] ( state ) {
		if(!state.quotation.status)
			state.quotation.status = state.choices.default_status[0]
	},

	[types.API_REQUEST] (state) {
		state.saving = true
	},

	[types.API_SUCCESS] (state) {
		state.saving = false
	},

	[types.API_FAILURE] (state, { response }) {
		console.log(response)
	},

	

	[types.SET_PAGINATION] (state, {count = 0, page = 1, page_size = 3}) {
		console.log(page_size)
		if(count){
			state.pagination.count = count
			state.pagination.page_size = page_size
		}
		state.pagination.current = Number(page)
	},
}

export default {
	state,
	getters,
	actions,
	mutations,
	// modules,
}
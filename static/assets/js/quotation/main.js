import Vue from 'vue'

Vue.config.debug = true

import Cookies from 'js-cookie'
var csrftoken = Cookies.get('csrftoken')

import axios from 'axios'
axios.defaults.headers.common['X-CSRFToken'] = csrftoken
Vue.prototype.$http = axios

Vue.directive('focus', {
	// When the bound element is inserted into the DOM...
	inserted: function (el) {
		// Focus the element
		el.focus()
	}
})
import filters from '../filters'
for(var p in filters) {
	Vue.filter(p, filters[p])
}
// import EnquiryDetail from './components/enquiry_detail.vue'
import App from './components/list.vue'

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.

new Vue({
	render: h => h(App),
}).$mount('#quotation-block')

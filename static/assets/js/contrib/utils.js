export default {
	isEmpty: function(obj) {
		for (var p in obj) {
			if (obj.hasOwnProperty(p))
				return false
		}
		return true
	},

	prepareQueryString: function(queries, refresh = false) {
		var url = ''
		// window.location.protocol + '//' + window.location.host + window.location.pathname
		console.log(!refresh, url)
		if(!refresh){ // if refresh is true then return url without search string
			console.log(queries)
			queries.forEach( (query, index) => {
				console.log(query)
				if(index === 0)
					url += '?' + query[0] + '=' + query[1]
				else
					url += '&' + query[0] + '=' + query[1]
			})
			// Object.keys(query).forEach( prop => {
			// 	console.log(prop, query[prop])
			// 	if(url.indexOf('?') > -1)
			// 		url += '&' + prop + '=' + query[prop]
			// 	else
			// 		url += '?' + prop + '=' + query[prop]
			// }) 
		}
		return url
	}
}
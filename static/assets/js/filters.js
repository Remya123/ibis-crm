import moment from 'moment'

export default {
	'human-date': function(value) {
		return moment(value).calendar(null, {
			sameDay: '[Today]',
			nextDay: '[Tomorrow]',
			nextWeek: 'dddd',
			lastDay: '[Yesterday]',
			lastWeek: '[Last] dddd',
			sameElse: 'DD MMM'
		})
			// return moment(value).format('dddd, MMMM Do YYYY, h:mm:ss a');
	},
	'rupees': function(value) {
		return '&#8377;' + value
	},
	'titleCase': function(str) {
		return str.toLowerCase().split(' ').map(function(word) {
			return word.replace(word[0], word[0].toUpperCase())
		}).join(' ')
	},
	'date': function(value, format){
		format = format || 'MMMM Do YYYY, h:mm:ss a'
		return moment(value).format(format)
		
	}

}

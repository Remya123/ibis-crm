from django.apps import AppConfig, apps as django_apps


class CrmConfig(AppConfig):
    name = 'crm'
    verbose_name = "Crm"

    def ready(self):

        from crm.models import CampaignCategory, Media

        Media.objects.update_or_create(name="News Paper")
        Media.objects.update_or_create(name="Television")
        Media.objects.update_or_create(name="Social Media")
        CampaignCategory.objects.update_or_create(name="Product Campaign")
        CampaignCategory.objects.update_or_create(name="Brand Campaign")


from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
# from invoice.models import Invoice
# from customer.models import Customer
import datetime
from erp_core.pagination import paginate
# from enquiry.models import Activity


def list(request, pk):

    date_check = request.GET.get('month_year')
    if date_check:
        date_for_check = datetime.datetime.strptime(date_check, "%B %Y").date()
    else:
        date_for_check = datetime.date.today()
    activities = Activity.objects.filter(
        enquiry__contact__pk=pk, date__month=date_for_check.month, date__year=date_for_check.year)
    activities, pagination = paginate(request, queryset=activities, per_page=20, default=True)

    context = {
        'activities': activities,
        'pk': pk,
        'pagination': pagination,
        'date_for_check': date_for_check,


    }
    return render(request, "crm/activity/list.html", context)

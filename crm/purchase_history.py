from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
# from invoice.models import Invoice
# from customer.models import Customer
import datetime
from erp_core.pagination import paginate


def purchases(request, pk):

    date_check = request.GET.get('month_year')
    if date_check:
        date_for_check = datetime.datetime.strptime(date_check, "%B %Y").date()
    else:
        date_for_check = datetime.date.today()
    purchases = Invoice.objects.filter(created_at__date__month=date_for_check.month, created_at__date__year=date_for_check.year)
    contacts, pagination = paginate(request, queryset=purchases, per_page=20, default=True)

    context = {
        'purchases': purchases,
        'pk': pk,
        'pagination': pagination,
        'date_for_check': date_for_check,


    }
    return render(request, "crm/purchase/list.html", context)

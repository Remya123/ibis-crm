
# from invoice.models import InvoicePaymentTerms
from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
# from invoice.models import Invoice
# from customer.models import Customer, Contact
from erp_core.pagination import paginate
# from crm.filters import CustomerFilter
import datetime
from student.models import Student


def dashboard(request):
    return render(request, "crm/dashboard.html")


# def customers(request):

#     f = CustomerFilter(request.GET)
#     contacts = Contact.objects.all()
#     contacts, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)

#     # print(f.form)
#     context = {
#         'contacts': contacts,
#         # 'status': Customer.STATUSES,
#         'pagination': pagination,
#         'filter': f,
#     }

#     return render(request, "crm/customers.html", context)


def full_payment_details(request, custm_pk):
    # contact = get_object_or_404(Contact, id=custm_pk)

    date_check = request.GET.get('month_year')
    if date_check:
        date_for_check = datetime.datetime.strptime(date_check, "%B %Y").date()
    else:

        date_for_check = datetime.date.today()

    payments = InvoicePaymentTerms.objects.filter(
        invoice__contact__pk=custm_pk,
        date__month=date_for_check.month,
        date__year=date_for_check.year
    )
    payments, pagination = paginate(request, queryset=payments, per_page=20, default=True)

    # payments = InvoicePaymentTerms.objects.filter(invoice__contact=contact)

    context = {
        # 'contact': contact,
        'payments': payments,
        'custm_pk': custm_pk,
        'pagination': pagination,
    }

    return render(request, "crm/full_payment_details.html", context)

from django.conf.urls import include, url
from crm import views, purchase_history, activity, campaign

app_name = "crm"

purchase_history_patterns = ([



    url(r'^student/(?P<pk>[^/]+)/purchase_history/$', purchase_history.purchases, name="purchases"),

], 'purchase_history')

activity_patterns = ([



    url(r'^student/(?P<pk>[^/]+)/activities/$', activity.list, name="list"),

], 'activity')

campaign_patterns = ([

    url(r'^create/$', campaign.campaign_create, name="campaign_create"),
    url(r'^list/$', campaign.campaign_list, name="campaign_list"),
    url(r'^(?P<pk>[^/]+)/detail/$', campaign.campaign_detail, name="campaign_detail"),
    url(r'^(?P<pk>[^/]+)/edit/$', campaign.campaign_edit, name="campaign_edit"),
    # url(r'^(?P<pk>[^/]+)/add_new_vendor/$', campaign.add_vendor, name="add_vendor"),
    # url(r'^(?P<pk>[^/]+)/add_vendor/$', campaign.existing_vendors, name="existing_vendors"),
    # url(r'^(?P<pk>[^/]+)/list_vendors/$', campaign.vendors, name="vendors"),
    # url(r'^vendor/(?P<pk>[^/]+)/remove/$', campaign.remove_vendor, name="remove_vendor"),
    url(r'^(?P<pk>[^/]+)/add_products/$', campaign.add_products, name="add_products"),
    url(r'^(?P<pk>[^/]+)/list_products/$', campaign.products, name="products"),
    url(r'^product/(?P<pk>[^/]+)/remove/$', campaign.remove_product, name="remove_product"),
    url(r'^TargetAudience/(?P<pk>[^/]+)/create/$', campaign.target_audience_create, name="target_audience_create"),
    url(r'^(?P<pk>[^/]+)/add_new_student/$', campaign.add_customer, name="add_customer"),
    url(r'^(?P<pk>[^/]+)/add_student/$', campaign.existing_customers, name="existing_customers"),
    url(r'^(?P<pk>[^/]+)/list_student/$', campaign.customers, name="customers"),
    url(r'^student/(?P<pk>[^/]+)/remove/$', campaign.remove_customer, name="remove_customer"),

], 'campaign')


urlpatterns = [

    url(r'^', include(purchase_history_patterns)),
    url(r'^', include(activity_patterns)),
    url(r'^dashboard/$', views.dashboard, name="dashboard"),
    # url(r'^students/$', views.customers, name="customers"),
    url(r'^campaign/', include(campaign_patterns)),
    # url(r'^crm/customers/$', views.customers, name="customers"),
    url(r'^crm/contact/(?P<custm_pk>[^/]+)/paymentDetails/$', views.full_payment_details, name='full_payment_details'),


]


from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
# import datetime
from erp_core.pagination import paginate
from crm.models import Campaign
from crm.filters import CampaignFilter
from .forms import *
# from project.models import Status, Project
# from vendor.models import Vendor, VendorContact
from django.forms import inlineformset_factory
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError, transaction
from student.models import Student




def campaign_list(request):
    from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
    page_number = request.GET.get('page',1)

    campaigns = Campaign.objects.all()
    f = CampaignFilter(request.GET, queryset=Campaign.objects.all().order_by("-created_at"))
    paginator = Paginator(f.qs,20)


    # import pdb; pdb.set_trace()
    try:
        campaigns = paginator.page(page_number)
    except PageNotAnInteger:
        campaigns = paginator.page(1)
    except EmptyPage:
        campaigns = paginator.page(paginator.num_pages)
    # campaigns, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)

    context = {

        'campaigns': campaigns,
        # 'pagination': pagination,
        'form': f.form,


    }
    return render(request, "crm/campaign/list.html", context)


def campaign_create(request):

    campaign_form = CampaignForm(request.POST or None)
    if request.method == "POST":
        if campaign_form.is_valid():
            campaign = campaign_form.save(commit=False)
            campaign.save()
            # createProject(campaign, request.user.employee)
            return HttpResponseRedirect(reverse('crm:campaign:campaign_detail', args=[campaign.id]))
    context = {
        "campaign_form": campaign_form,
        'form_url': reverse_lazy('crm:campaign:campaign_create'),
        "type": "create"
    }
    return render(request, 'crm/campaign/edit.html', context)


def campaign_detail(request, pk):
    campaign = Campaign.objects.get(id=pk)
    context = {
        "campaign": campaign,
        "pk": pk
    }
    return render(request, "crm/campaign/detail.html", context)


def campaign_edit(request, pk):

    campaign = get_object_or_404(Campaign, id=pk)
    campaign_form = CampaignForm(
        request.POST or None, instance=campaign)
    if request.method == "POST":
        if campaign_form.is_valid():
            campaign = campaign_form.save(commit=False)
            campaign.save()
            return HttpResponseRedirect(reverse('crm:campaign:campaign_detail', args=[campaign.id]))
    context = {
        "campaign_form": campaign_form,
        'form_url': reverse_lazy('crm:campaign:campaign_edit', args=[campaign.pk]),
        "type": "edit",
    }
    return render(request, 'crm/campaign/edit.html', context)


# def createProject(campaign, employee):
#     project_count = Project.objects.count() + 1
#     print(project_count)
#     status = get_object_or_404(Status, name__iexact="NOT YET STARTED")
#     # status = Status.objects.get(name=Status.STATUSES.NOT_YET_STARTED)
#     print(status)
#     code = "CAMP_PROJ" + str(project_count)
#     project = Project.objects.create(name=campaign.name, code=code, status=status,
#                                      actual_start_date=campaign.start_date, actual_end_date=campaign.end_date, created_by=employee)
#     campaign.project = project
#     campaign.save()


# @transaction.atomic
# def add_vendor(request, pk):
#     campaign = get_object_or_404(Campaign, id=pk)

#     ContactInlineFormSet = inlineformset_factory(
#         Vendor, VendorContact, form=VendorContactForm, extra=0, can_delete=True, min_num=1, validate_min=True)
#     contact_formset = ContactInlineFormSet(request.POST or None)

#     vendor_form = VendorForm(request.POST or None)
#     publisher_form = CampaignPublisherForm(request.POST or None)
#     # print(contact_formset)
#     if request.method == "POST":

#         if vendor_form.is_valid() and contact_formset.is_valid() and publisher_form.is_valid():
#             vendor = vendor_form.save(commit=False)
#             vendor.campaign_vendor = True
#             contacts = contact_formset.save(commit=False)
#             for contact in contacts:
#                 contact.vendor = vendor
#                 contact.save()
#             vendor.save()
#             vendor_form.save_m2m()
#             campaign_ob = publisher_form.save(commit=False)
#             campaign_ob.vendor = vendor
#             campaign_ob.campaign = campaign
#             campaign_ob.save()
#             messages.add_message(request, messages.INFO, 'New vendor added')

#             return HttpResponseRedirect(reverse('crm:campaign:vendors', args=[campaign.pk]))

#     context = {
#         'vendor_form': vendor_form,
#         'contact_formset': contact_formset,
#         'publisher_form': publisher_form,
#         'form_url': reverse_lazy('crm:campaign:add_vendor', args=[campaign.pk]),
#         'type': "Create",
#         'campaign': campaign,

#     }
#     return render(request, "crm/campaign/create_vendor.html", context)


# @transaction.atomic
# def existing_vendors(request, pk):
#     campaign = get_object_or_404(Campaign, id=pk)

#     vendor_form = CampaignVendorForm(request.POST or None)
#     # print(contact_formset)
#     if request.method == "POST":

#         if vendor_form.is_valid():
#             try:
#                 with transaction.atomic():
#                     vendor = vendor_form.save(commit=False)
#                     vendor.campaign = campaign
#                     vendor.save()
#                     messages.add_message(request, messages.INFO, 'New vendor added')
#                     return HttpResponseRedirect(reverse('crm:campaign:vendors', args=[campaign.pk]))
#             except IntegrityError:
#                 messages.add_message(request, messages.INFO, 'This vendor allready added')

#                 return HttpResponseRedirect(reverse('crm:campaign:existing_vendors', args=[campaign.pk]))

#     context = {
#         'vendor_form': vendor_form,
#         'form_url': reverse_lazy('crm:campaign:existing_vendors', args=[campaign.pk]),
#         'type': "Create",
#         'campaign': campaign,

#     }
#     return render(request, "crm/campaign/existing_vendors.html", context)


# def vendors(request, pk):
#     campaign = Campaign.objects.get(id=pk)
#     context = {
#         "campaign": campaign,
#     }
#     return render(request, "crm/campaign/vendors.html", context)


# def remove_vendor(request, pk):
#     if request.method == "POST":

#         vendor = CampaignVendor.objects.get(id=pk)
#         vendor.delete()
#         return HttpResponseRedirect(reverse('crm:campaign:vendors', args=[vendor.campaign.pk]))


@transaction.atomic
def add_products(request, pk):
    campaign = get_object_or_404(Campaign, id=pk)

    product_form = CampaignProductForm(request.POST or None)
    # print(contact_formset)
    if request.method == "POST":

        if product_form.is_valid():
            try:
                with transaction.atomic():
                    product = product_form.save(commit=False)
                    product.campaign = campaign
                    product.save()
                    messages.add_message(request, messages.INFO, 'New prodcut added')
                    return HttpResponseRedirect(reverse('crm:campaign:products', args=[campaign.pk]))
            except IntegrityError:
                messages.add_message(request, messages.INFO, 'This product allready added')

                return HttpResponseRedirect(reverse('crm:campaign:add_products', args=[campaign.pk]))

    context = {
        'product_form': product_form,
        'form_url': reverse_lazy('crm:campaign:add_products', args=[campaign.pk]),
        'type': "Create",
        'campaign': campaign,

    }
    return render(request, "crm/campaign/add_products.html", context)


def products(request, pk):
    campaign = Campaign.objects.get(id=pk)
    context = {
        "campaign": campaign,
    }
    return render(request, "crm/campaign/products.html", context)


def remove_product(request, pk):
    if request.method == "POST":

        product = CampaignProduct.objects.get(id=pk)
        product.delete()
        return HttpResponseRedirect(reverse('crm:campaign:products', args=[product.campaign.pk]))


def target_audience_create(request, pk):
    campaign = Campaign.objects.get(id=pk)
    if TargetAudienceCampaign.objects.filter(campaign=campaign).exists():
        target_aud = TargetAudienceCampaign.objects.get(campaign=campaign)
        target_audience_form = TargetAudienceCampaignForm(request.POST or None, instance=target_aud)
    else:
        target_audience_form = TargetAudienceCampaignForm(request.POST or None)
    if request.method == "POST":
        if target_audience_form.is_valid():
            target_audience = target_audience_form.save(commit=False)
            target_audience.campaign = campaign
            target_audience.save()
            messages.add_message(request, messages.INFO, 'Target Audience is Added Successfully....!')
            return HttpResponseRedirect(reverse('crm:campaign:campaign_detail', args=[campaign.id]))
    context = {
        "target_audience_form": target_audience_form,
        'form_url': reverse_lazy('crm:campaign:target_audience_create', args=[campaign.id]),
        "type": "create",
        'campaign': campaign,
    }
    return render(request, 'crm/campaign/target_audience_edit.html', context)


@transaction.atomic
def add_customer(request, pk):
    campaign = get_object_or_404(Campaign, id=pk)

    ContactInlineFormSet = inlineformset_factory(Customer, Contact, form=ContactForm, extra=0,
                                                 can_delete=True, min_num=1, max_num=1, validate_min=True)
    contact_formset = ContactInlineFormSet(request.POST or None)
    QualificationInlineFormSet = inlineformset_factory(Customer, QualificationForCustomer, form=QualificationCreationForm, extra=0,
                                                       can_delete=True, min_num=1, validate_min=True)
    qualification_formset = QualificationInlineFormSet(request.POST or None)
    DocumentInlineFormSet = inlineformset_factory(Customer, DocumentsForCustomer, form=DocumentCreationForm, extra=0,
                                                  can_delete=True, min_num=1, validate_min=True)
    document_formset = DocumentInlineFormSet(request.POST or None, request.FILES or None)

    customer_form = CustomerForm(request.POST or None)
    user_form = UserForm(request.POST or None)
    address_form = ContactAddressForm(request.POST or None)

    if request.method == "POST":

        if user_form.is_valid() and customer_form.is_valid() and contact_formset.is_valid() and qualification_formset.is_valid() and document_formset.is_valid():
            user = user_form.save(commit=False)
            user.save()
            customer = customer_form.save(commit=False)
            customer.user = user
            customer.save()

            contacts = contact_formset.save(commit=False)
            for contact in contacts:
                contact.customer = customer
                contact.save()

                address = address_form.save(commit=False)
                address.contact = contact
                address.save()
            qualifications = qualification_formset.save(commit=False)
            for qualification in qualifications:
                qualification.customer = customer
                qualification.save()
            documents = document_formset.save(commit=False)

            for document in documents:
                document.customer = customer
                document.save()

            CampaignCustomer.objects.create(campaign=campaign, customer=customer)
            messages.add_message(request, messages.INFO, 'New customer added')
            return HttpResponseRedirect(reverse('crm:campaign:customers', args=[campaign.pk]))

    context = {
        'user_form': user_form,
        'customer_form': customer_form,
        'contact_formset': contact_formset,
        'address_form': address_form,
        'qualification_formset': qualification_formset,
        'document_formset': document_formset,
        'form_url': reverse_lazy('crm:campaign:add_customer', args=[campaign.pk]),
        'type': "Create",
        'campaign': campaign,

    }
    return render(request, "customer/edit.html", context)

@transaction.atomic
def existing_customers(request, pk):
    campaign = get_object_or_404(Campaign, id=pk)

    customer_form = CampaignCustomerForm(request.POST or None)
    # print(contact_formset)
    if request.method == "POST":

        if customer_form.is_valid():
            try:
                with transaction.atomic():
                    customer = customer_form.save(commit=False)
                    customer.campaign = campaign
                    customer.save()
                    messages.add_message(request, messages.INFO, 'New customer added')
                    return HttpResponseRedirect(reverse('crm:campaign:customers', args=[campaign.pk]))
            except IntegrityError:
                messages.add_message(request, messages.INFO, 'This vendor allready added')

                return HttpResponseRedirect(reverse('crm:campaign:existing_customers', args=[campaign.pk]))

    context = {
        'customer_form': customer_form,
        'form_url': reverse_lazy('crm:campaign:existing_customers', args=[campaign.pk]),
        'type': "Create",
        'campaign': campaign,

    }
    return render(request, "crm/campaign/exsting_customers.html", context)


def customers(request, pk):
    students = Student.objects.filter(campaign__pk=pk)
    campaign = get_object_or_404(Campaign,pk=pk)
    context = {
        "students": students,
        'campaign':campaign,
    }
    return render(request, "crm/campaign/customers.html", context)


def remove_customer(request, pk):
    if request.method == "POST":
        student = Student.objects.get(id=pk)
        campaign = get_object_or_404(Campaign,pk=student.campaign.pk)
        student.campaign=None
        student.save()
        messages.add_message(request, messages.INFO, 'Stuent removed')
        return HttpResponseRedirect(reverse('crm:campaign:customers', args=[campaign.pk]))

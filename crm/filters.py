import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from crm.models import Campaign, Media, CampaignCategory


# class CustomerFilter(django_filters.FilterSet):

#     name = django_filters.CharFilter(label="Contact name", lookup_expr='icontains',
#                                      widget=forms.TextInput(attrs={'class': 'form-control'}))
#     contact = django_filters.CharFilter(label="Company name", method='filter_by_contact',
#                                         widget=forms.TextInput(attrs={'class': 'form-control'}))

#     class Meta:
#         model = Customer
#         fields = ['name', ]

#     def filter_by_contact(self, queryset, name, value):
#         return queryset.filter(
#             Q(customer__name__icontains=value)
#         )


class CampaignFilter(django_filters.FilterSet):

    name = django_filters.CharFilter(label="Campaign Title", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))
    media_type = django_filters.ChoiceFilter(choices=Campaign.TYPES, label="Media Type",
                                             widget=forms.Select(attrs={'class': 'form-control'}))
    media = django_filters.ModelChoiceFilter(
        queryset=Media.objects.all(),
        # method='filter_by_city',
        name="media",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", }),
        label="Media"
    )
    campaign = django_filters.ModelChoiceFilter(
        queryset=Campaign.objects.all(),
        # method='filter_by_city',
        name="name",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", }),
        label="Media"
    )

    class Meta:
        model = Campaign
        fields = ['name', ]

    def filter_by_contact(self, queryset, name, value):
        return queryset.filter(
            Q(customer__name__icontains=value)
        )

from django.db import models

from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator
from location.models import City
# from customer.models import Contact, Customer
# from project.models import Project
# Create your models here.


class Media(ErpAbstractBaseModel):

    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class CampaignCategory(ErpAbstractBaseModel):

    name = models.CharField(max_length=100)
    # eg: print,ad

    def __str__(self):
        return str(self.name)


class Campaign(ErpAbstractBaseModel):
    ONLINE = 'online'
    OFFLINE = 'offline'
    TYPES = (
            (ONLINE, "online"),
            (OFFLINE, "offline"),
    )
    name = models.CharField(max_length=255)
    media_type = models.CharField(max_length=30, choices=TYPES, default=TYPES[0])
    budget = models.DecimalField(max_digits=20, decimal_places=2, default=0, validators=[MinValueValidator(0)])
    description = models.TextField()
    offer_details = models.TextField(null=True, blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    no_of_responses = models.PositiveIntegerField(null=True, blank=True, default=0)
    no_of_conversions = models.PositiveIntegerField(null=True, blank=True, default=0)
    group = models.TextField('Campaign Group', null=True, blank=True)  # onam

    media = models.ForeignKey(Media, on_delete=models.CASCADE)
    category = models.ForeignKey(CampaignCategory, on_delete=models.CASCADE)
    location = models.ForeignKey(City, on_delete=models.CASCADE)
    # project = models.OneToOneField(Project, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class CampaignVendor(ErpAbstractBaseModel):
    publisher = models.BooleanField(default=False)

    campaign = models.ForeignKey(Campaign, related_name="%(class)ss", on_delete=models.CASCADE)

    # class Meta:
    #     unique_together = (("campaign", "vendor"),)

    def __str__(self):
        return str(self.campaign)


# class CampaignProduct(ErpAbstractBaseModel):
#     campaign = models.ForeignKey(Campaign, related_name="%(class)ss", on_delete=models.CASCADE)
#     product = models.ForeignKey(Product, on_delete=models.CASCADE)

#     class Meta:
#         unique_together = (("campaign", "product"),)

#     def __str__(self):
#         return str(self.campaign)


class TargetAudienceCampaign(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
        ('Others', 'Others'),
    )
    gender = models.CharField(max_length=8, choices=SEX)
    age_limit = models.CharField(max_length=8)
    income_range = models.PositiveIntegerField()

    campaign = models.OneToOneField(Campaign, related_name="audience", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.campaign)


# class CampaignCustomer(ErpAbstractBaseModel):
#     campaign = models.ForeignKey(Campaign, related_name="campaign_customers", on_delete=models.CASCADE)
#     customer = models.ForeignKey(Customer, related_name="campaign_sets", on_delete=models.CASCADE)

#     def __str__(self):
#         return str(self.campaign)

#     class Meta:
#         unique_together = (("campaign", "customer"),)

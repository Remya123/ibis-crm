from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Campaigns',
            'crm:campaign:campaign_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="newspaper-o",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),
    # ... You can define more than one tree for your app.
)

from django import forms
from django.forms import ModelForm
from .models import *


class CampaignForm(ModelForm):

    class Meta:
        model = Campaign
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "media_type": forms.Select(attrs={'class': "form-control"}),
            "budget": forms.NumberInput(attrs={'class': "form-control"}),
            "description": forms.Textarea(attrs={'class': "form-control", 'rows': 3}),
            "offer_details": forms.Textarea(attrs={'class': "form-control", 'rows': 3}),
            "start_date": forms.DateInput(attrs={'class': "form-control tdate", 'id': "start_date"}),
            "end_date": forms.DateInput(attrs={'class': "form-control tdate", 'id': "end_date"}),
            # "no_of_responses": forms.NumberInput(attrs={'class': "form-control"}),
            # "no_of_conversions": forms.NumberInput(attrs={'class': "form-control"}),
            # "group": forms.TextInput(attrs={'class': "form-control"}),
            "media": forms.Select(attrs={'class': "form-control"}),
            "category": forms.Select(attrs={'class': "form-control"}),
            "location": forms.Select(attrs={'class': "form-control"}),

        }
        fields = ['name', 'media_type', 'budget', 'description', 'offer_details', 'start_date', 'end_date','media', 'category', 'location']


class CampaignVendorForm(ModelForm):

    class Meta:
        model = CampaignVendor
        widgets = {
            "publisher": forms.CheckboxInput(attrs={}),
        }
        fields = ['campaign', ]


class CampaignPublisherForm(ModelForm):

    class Meta:
        model = CampaignVendor
        widgets = {
            "publisher": forms.CheckboxInput(attrs={}),


        }
        fields = ['publisher', ]


# class CampaignProductForm(ModelForm):

#     class Meta:
#         model = CampaignProduct
#         widgets = {
#             "product": forms.Select(attrs={'class': "form-control"}),


#         }
#         fields = ['product', ]


class TargetAudienceCampaignForm(ModelForm):

    class Meta:
        model = TargetAudienceCampaign
        widgets = {
            "gender": forms.Select(attrs={'class': "form-control"}),
            "income_range": forms.NumberInput(attrs={'class': "form-control"}),
            "age_limit": forms.TextInput(attrs={'class': "form-control"}),
        }
        fields = ['gender', 'income_range', 'age_limit']


# class CampaignCustomerForm(ModelForm):

#     class Meta:
#         model = CampaignCustomer
#         widgets = {
#             "customer": forms.Select(attrs={'class': "form-control", 'required': "required"}),


#         }
#         exclude = ['campaign', ]

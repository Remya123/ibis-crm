from django.contrib import admin
from crm.models import Campaign, CampaignCategory, Media

# admin.site.register(Campaign)
admin.site.register(Media)
admin.site.register(CampaignCategory)
# admin.site.register(CampaignVendor)
# admin.site.register(CampaignCustomer)
# admin.site.register(CampaignProduct)


from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import get_object_or_404, render
from enquiry.models import Enquiry

# Create your views here.



@login_required
def dashboard(request):

    context = {


    }
    return render(request, "erp_admin/dashboard.html", context)

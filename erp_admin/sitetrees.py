from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('account', items=[
        # Then define items and their children with `item` function.
        item(
            '{{ user.username }}',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            children=[
                item(
                    'Profile',
                    'employee:profile user.employee.code',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="user",
                ),
                item(
                    'Settings',
                    'admin:index',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="indent",
                ),
                item(
                    'Change Password',
                    'account_change_password',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="key",
                ),
                item(
                    'Logout',
                    'account_logout',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="sign-out",
                ),


            ]),
    ]),

    tree('customer_account', items=[
        # Then define items and their children with `item` function.
        item(
            '{{ user.username }}',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            children=[

                item(
                    'Logout',
                    'account_logout',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="sign-out",
                ),


            ]),
    ]),


)

from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect


# from customer.models import Customer


# from django.contrib.auth.models import User

class AccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request, success_url=None):

        # if success_url is not None:

        #     return success_url
        if request.user.groups.filter(name__iexact='employee').count() == 1:
            return reverse('employee:profile', args=[request.user.employee.code])
        elif request.user.groups.filter(name__iexact='customer').count() == 1:
            return reverse('customer:detail', args=[request.user.customer.pk])

        elif request.user.groups.filter(name__iexact='agent').count() == 1:
            return reverse('enquiry:list')

            # elif request.user.groups.filter(name='canteen_user').count() == 1:
            #     return ''
            # elif request.user.groups.filter(name='Theatre Employee').count() == 1:
            #     return '/theatre'
            # elif request.user.groups.filter(name='Customer').count() == 1:
            #     return reverse('home')
            #     # return reverse('', args=[request.user.username])
        else:
            return reverse('enquiry:list')

    # def ajax_response(self, request, response, redirect_to=None, form=None):
    #     data = {}
    #     status = response.status_code

    #     if redirect_to:
    #         status = 200
    #         data['location'] = redirect_to
    #     if form:
    #         if form.is_valid():
    #             status = 200
    #         else:
    #             status = 400
    #             data['form_errors'] = form._errors
    # if request.method == 'GET':
    # if hasattr(response, 'render'):
    # response.render()
    # data['html'] = response.content.decode('utf8')
    #     return HttpResponse(json.dumps(data),
    #                         status=status,
    #                         content_type='application/json')

    # def save_user(self, request, user, form, commit=True):
    #     """
    #     Saves a new `User` instance using information provided in the
    #     signup form.
    #     """
    #     from allauth.account.utils import user_username, user_email, user_field
    #
    #     data = form.cleaned_data
    #     first_name = data.get('first_name')
    #     last_name = data.get('last_name')
    #     email = data.get('email')
    #     username = data.get('username')
    #     user_email(user, email)
    #     user_username(user, username)
    #     if first_name:
    #         user_field(user, 'first_name', first_name)
    #     if last_name:
    #         user_field(user, 'last_name', last_name)
    #     if 'password1' in data:
    #         user.set_password(data["password1"])
    #     else:
    #         user.set_unusable_password()
    #     self.populate_username(request, user)
    #     if commit:
    #         # Ability not to commit makes it easier to derive from
    #         # this adapter by adding
    #         try:
    #             user.save()
    #             customer_group = Group.objects.get(name='Customer')
    #             user.groups.add(customer_group)
    #             customer = Customer(user=user)
    #             customer.save()
    #         except Group.DoesNotExist:
    #             pass
    #     return user

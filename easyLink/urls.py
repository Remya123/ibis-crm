from django.conf import settings
from django.conf.urls import include, url
"""easyLink URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from sitetree.sitetreeapp import compose_dynamic_tree, register_dynamic_trees
from django.conf.urls.static import static
from erp_admin import views as erpAdminViews

urlpatterns = [
    url(r'^$', erpAdminViews.dashboard),
    url(r'^admin/', admin.site.urls),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^', include('employee.urls', namespace="employee")),
    # url(r'^', include('customer.urls', namespace="customer")),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^', include('product.urls', namespace="product")),
    url(r'^crm/', include('crm.urls', namespace="crm")),
    url(r'^', include('enquiry.urls', namespace="enquiry")),
    url(r'^erp_core/', include('erp_core.urls', namespace="erp_core")),
    url(r'^', include('student.urls', namespace="student")),
    url(r'^', include('location.urls', namespace="location")),



] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
register_dynamic_trees(

    compose_dynamic_tree('erp_admin'),

    # or gather all the trees from `books` and attach them to `main` tree root,
    # compose_dynamic_tree('project'),
    # compose_dynamic_tree('company'),

    # compose_dynamic_tree('gst'),
    compose_dynamic_tree('employee'),
    # compose_dynamic_tree('product'),
    # compose_dynamic_tree('vendor'),
    compose_dynamic_tree('product'),

    # compose_dynamic_tree('inventory'),
    # compose_dynamic_tree('customer'),
    compose_dynamic_tree('enquiry'),
    compose_dynamic_tree('crm'),
    compose_dynamic_tree('student'),


)

# from django.views.decorators.csrf import ensure_csrf_cookie
# from django.utils.decorators import method_decorator
# from django.db.models import Prefetch
# # from rest_framework.renderers import TemplateHTMLRenderer
# from rest_framework.response import Response
# from rest_framework import generics
# from rest_framework.views import APIView

# from enquiry.serializers import EnquirySerializer, EnquiryViewSerializer, ActivityTypeSerializer
# from enquiry.models import Enquiry, Item, ActivityType
# from product.models import Product
# from product.serializers import ProductViewSerializer
# # from customer.serializers import CustomerSerializer


# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class EnquiryList(generics.ListAPIView):
#     """
#     A simple ViewSet for viewing Enquirys.
#     """
#     queryset = Enquiry.objects.all()
#     serializer_class = EnquiryViewSerializer

#     def list(self, request):
#         #print("list")
#         response = super(EnquiryList, self).list(request)
#         response.template_name = "enquiry/index.html"
#         return response


# class EnquiryCreate(generics.CreateAPIView):
#     queryset = Enquiry.objects.all()
#     serializer_class = EnquirySerializer

# from django.utils.decorators import method_decorator
# from django.views.decorators.csrf import ensure_csrf_cookie
# from django.urls import reverse
# from django.conf import settings
# from django.utils.dateparse import parse_datetime
# import pytz
# from rest_framework import viewsets, status
# from rest_framework.decorators import list_route, detail_route
# from rest_framework.response import Response
# from enquiry.filters import EnquiryFilter
# from enquiry.models import Enquiry, ActivityType, CodeSetting
# from enquiry.serializers import EnquirySerializer, EnquiryViewSerializer, ActivityTypeSerializer
# from product.models import Product
# from product.serializers import ProductViewSerializer
# from globalconfig.models import GlobalConfigSerializer, GlobalConfig
# from employee.serializers import EmployeeSerializer


# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class EnquiryViewSet(viewsets.ModelViewSet):
#     serializer_class = EnquirySerializer
#     # renderer_classes = (TemplateHTMLRenderer,)
#     ordering_fields = ('created_at', 'updated_at', 'amount')
#     filter_class = EnquiryFilter  # ordering = ('-updated_at')

#     def get_queryset(self):
#         queryset = Enquiry.objects.prefetch_related("items", "activities").order_by('-id')
#         return queryset

#     def get_serializer_class(self, *args, **kwargs):
#         if self.request.method == "GET":
#             self.serializer_class = EnquiryViewSerializer
#         return super(EnquiryViewSet, self).get_serializer_class()

#     def list(self, request, *args, **kwargs):
#         response = super(EnquiryViewSet, self).list(request)
#         response.template_name = "enquiry/index_new.html"
#         return response

#     def create(self, request, *args, **kwargs):

#         response = super(EnquiryViewSet, self).create(request)

#         response.data['success_url'] = reverse('enquiry:detail', args=[response.data['id']])
#         return response

#     def perform_create(self, serializer):
#         data = serializer.initial_data
#         follow_up_date = data['activities'][0]['date']
#         # naive = parse_datetime(follow_up_date)
#         # pytz.timezone(settings.TIME_ZONE).localize(naive, is_dst=None)
#         return serializer.save(follow_up_date=follow_up_date)
#         # status=Enquiry.STATUSES[0][0]

#     def retrieve(self, request, *args, **kwargs):
#         response = super(EnquiryViewSet, self).retrieve(request)
#         response.template_name = "enquiry/detail.html"
#         # response.data = self.get_object()
#         return response
#         # return Response({"enquiry": self.get_object()}, template_name=template_name)

#         # def update(self, request, pk=None):
#         #     response = super(EnquiryViewSet, self).update(request)
#         #     return response
#         # return Response(context, template_name="enquiry/index.html")
#     def update(self, request, *args, **kwargs):
#         response = super(EnquiryViewSet, self).update(request)
#         return response

#     @list_route()
#     def choices(self, request):
#         products = ProductViewSerializer(Product.objects.all(), many=True).data
#         global_config = GlobalConfigSerializer(GlobalConfig.objects.first()).data
#         activity_types = ActivityTypeSerializer(ActivityType.objects.all(), many=True).data
#         employee = EmployeeSerializer(request.user.employee).data
#         context = {
#             "types": Enquiry.TYPES,
#             "default_type": Enquiry._meta.get_field('type').get_default(),
#             "statuses": Enquiry.STATUSES,
#             "default_status": Enquiry._meta.get_field('status').get_default(),
#             "priorities": Enquiry.PRIORITIES,
#             "default_priority": Enquiry._meta.get_field('priority').get_default(),
#             "products": products,
#             "activity_types": activity_types,
#             "global_config": global_config,
#             "employee": employee,
#             "code": CodeSetting.new_code()
#         }
#         return Response(context)

#     @list_route()
#     def create_form(self, request):
#         response = Response()
#         response.template_name = "enquiry/edit.html"
#         return response

#     @detail_route()
#     def detail(self, request):
#         response = Response()
#         response.template_name = "enquiry/edit.html"
#         return response

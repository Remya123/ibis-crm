import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from crm.models import Campaign
from enquiry.models import Enquiry
from location.models import Office
from django.contrib.auth.models import User
from product.models import Program


class EnquiryFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(name="code", lookup_expr='contains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    name_of_candidate = django_filters.CharFilter(name="name_of_candidate", lookup_expr='contains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    year_of_passout = django_filters.NumberFilter(name="year_of_passout", lookup_expr='iexact', widget=forms.TextInput(attrs={'class': 'form-control'}))
    course = django_filters.ModelChoiceFilter(
        queryset=Program.objects.all(),
        # name='',
        method='filter_by_program',
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )

    class Meta:
        model = Enquiry
        fields = ['code', ]

    def filter_by_program(self, queryset, name, value):
        return queryset.filter(
            Q(course__name__icontains=value)
        )

# class WebEnquiryFilter(django_filters.FilterSet):
#     code = django_filters.CharFilter(lookup_expr='iexact', method='filter_by_code', widget=forms.TextInput(attrs={'class': 'form-control'}))

#     amount = django_filters.NumberFilter(widget=forms.Select(attrs={'class': 'form-control'}))

#     min_amount = django_filters.NumberFilter(name='amount', lookup_expr='gte',
#                                              widget=forms.Select(attrs={'class': 'form-control'}))
#     max_amount = django_filters.NumberFilter(name='amount', lookup_expr='lte',
#                                              widget=forms.Select(attrs={'class': 'form-control'}))

#     contact = django_filters.CharFilter(label="Contact", method='filter_by_contact',
#                                         widget=forms.TextInput(attrs={'class': 'form-control'}))

#     # status = django_filters.ChoiceFilter(choices=Enquiry.STATUSES, widget=forms.Select(attrs={'class': 'form-control'}))
#     status = django_filters.ModelChoiceFilter(
#         queryset=Status.objects.all(),
#         method='filter_by_status',
#         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
#     )

#     type = django_filters.ChoiceFilter(choices=Enquiry.TYPES, widget=forms.Select(attrs={'class': 'form-control'}))
#     priority = django_filters.ChoiceFilter(
#         choices=Enquiry.PRIORITIES, widget=forms.Select(attrs={'class': 'form-control'}))

#     agency = django_filters.ModelChoiceFilter(
#         queryset=User.objects.all(),
#         name='agency',
#         method= 'filter_by_agent',
#         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
#     )


#     class Meta:
#         model = Enquiry
#         fields = ['code', 'type', 'status', 'min_amount', 'max_amount', 'contact', 'created_at', 'agency']

#     def filter_by_contact(self, queryset, name, value):
#         return queryset.filter(
#             Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
#         )

#     def order_by_field(self, queryset, name, value):
#         return queryset.order_by(value)

#     def filter_by_status(self, queryset, name, value):
#         return queryset.filter(
#             Q(status__name=value)
#         )
#     def filter_by_code(self, queryset, name, value):
#         return queryset.filter(
#             Q(customer__code__icontains=value)
#         )

#     def filter_by_agent(self, queryset, name, value):
#         return queryset.filter(
#             Q(customer__refered_by=value)
#         )

# class OfficeFilter(django_filters.FilterSet):
#     office = django_filters.ModelChoiceFilter(
#         queryset=Office.objects.all(),
#         method='filter_by_office',
#         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
#     )
#     min_amount = django_filters.NumberFilter(name="amount", lookup_expr='gte')
#     max_amount = django_filters.NumberFilter(name="amount", lookup_expr='lte')
#     contact = django_filters.CharFilter(name="contact__name")
#     type = django_filters.MultipleChoiceFilter(choices=Enquiry.TYPES)

#     class Meta:
#         model = Enquiry
#         fields = ['type', 'status', 'min_amount', 'max_amount', 'contact']

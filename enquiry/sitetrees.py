
from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Leads',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="clipboard",
            # access_by_perms=['project.view_project_list'],

        ),


    ]),
    tree('sidebar_counselor', items=[
        # Then define items and their children with `item` function.
        item(
            'Leads',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="clipboard",
            # access_by_perms=['project.view_project_list'],

        ),


    ]),
    tree('sidebar_referral_agent', items=[
        # Then define items and their children with `item` function.
        item(
            'Leads',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="clipboard",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),
    # ... You can define more than one tree for your app.
)

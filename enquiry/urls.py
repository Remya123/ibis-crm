from django.conf.urls import url, include
from enquiry import api, views, mobile_api, enquiry
from rest_framework import routers

# router = routers.DefaultRouter()
# router.register(r'enquiries', api.EnquiryViewSet, base_name="enquiry")

# m_router = routers.DefaultRouter()
# activity_router = routers.DefaultRouter()
# m_router.register(r'enquiries', mobile_api.EnquiryViewSet, base_name="enquiry")
# activity_router.register(r'activities', activity_api.ActivityViewSet, base_name="activities")

# enquiry_list = api.EnquiryViewSet.as_view({'get': 'list'})
# enquiry_create = api.EnquiryViewSet.as_view({'get': 'create_form'})
# enquiry_retrieve = api.EnquiryViewSet.as_view({'get': 'retrieve'})

# activity_list = activity_api.ActivityViewSet.as_view({'get': 'list'})
# activity_detail = activity_api.ActivityViewSet.as_view({'get': 'retrieve'})
# quotation_list = quote.QuotationViewSet.as_view({'get': 'list'})

app_name = "enquiry"

urlpatterns = [
    # url(r'^enquiries/', enquiry_api.enquiry_list, name="enquiry_list"),
    url(r'^enquiries/$', enquiry.list, name="list"),
    url(r'^enquiries/create/$', enquiry.create, name="create"),
    url(r'^enquiries/code_settings/$', views.code_settings, name='code_settings'),
    # url(r'^enquiries/(?P<pk>[^/]+)/$', enquiry_retrieve, name="detail"),
    url(r'^enquiries/(?P<pk>[^/]+)/delete/$', enquiry.delete, name="delete"),
    url(r'^enquiry/(?P<pk>[^/]+)/details/$', enquiry.enquiry_detail, name="enquiry_detail"),
    url(r'^enquiry/(?P<pk>[^/]+)/edit/$', enquiry.edit, name="edit"),
    url(r'^enquiry/activity/(?P<pk>[^/]+)/complete/$', enquiry.complete_activity, name="complete_activity"),
    url(r'^enquiry/(?P<pk>[^/]+)/edit_status/$', enquiry.edit_status, name="edit_status"),
    url(r'^enquiry/(?P<pk>[^/]+)/remark/$', enquiry.add_remark, name="add_remark"),
    url(r'^enquiry/(?P<pk>[^/]+)/onhold/$', enquiry.add_onhold_remark, name="add_onhold_remark"),


    # /enquiries/code_settings/
    # Activity urls
    url(r'^enquiries/(?P<enquiry_pk>[^/]+)/activities/create/$', views.create_activity, name="add_activity"),
    url(r'^activitie/(?P<pk>[^/]+)/edit/$', views.edit_activity, name="edit_activity"),
    url(r'^activitie/(?P<pk>[^/]+)/delete/$', views.delete_activity, name="delete_activity"),
    url(r'^activitie/(?P<pk>[^/]+)/view/$', views.view_activity, name="view_activity"),
    # url(r'^quotations/$', quotation_list, name="quotations"),
    # url(r'^api/', include(router.urls, namespace='api')),
    # url(r'^m/api/', include(m_router.urls, namespace='m-api')),
    # url(r'^api/', include(activity_router.urls, namespace='m-api')),
    # url(r'^quote/', include(router.urls, namespace='quote')),
    url(r'^reports/office/$', enquiry.filter_based_on_office, name="filter_based_on_office"),
    url(r'^reports/streamwise/$', enquiry.stream_wise_report, name="stream_wise_report"),
    url(r'^enquiries/(?P<pk>[^/]+)/activity_type/$', views.create_activity_type, name="create_activity_type"),


]
# GET /m/api/enquiries/ => enquiry list
# GET /m/api/enquiries/choices => enquiry choice fields
# POST /m/api/enquiries/  => create enquiry
# PUT /m/api/enquiries/{pk}  => update enquiry
# DELETE /m/api/enquiries/{pk}  => delete enquiry

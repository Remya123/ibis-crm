# from django.db import transaction

# from rest_framework import serializers
# from enquiry.models import Enquiry, Item, Activity, ActivityType
# from employee.serializers import EmployeeSerializer
# from customer.serializers import ContactViewSerializer
# from customer.models import Contact
# # from customer.models import Customer
# # from django.shortcuts import get_object_or_404
# from product.serializers import ProductViewSerializer
# from product.models import Product


# class ActivityTypeSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = ActivityType
#         fields = ('id', 'name')


# class ActivityViewSerializer(serializers.ModelSerializer):
#     type = ActivityTypeSerializer()

#     class Meta:
#         model = Activity
#         fields = ('id', 'description', 'date', 'type')


# class ActivitySerializer(serializers.ModelSerializer):
#     # description = serializers.CharField(required=True)

#     class Meta:
#         model = Activity
#         fields = ('id', 'description', 'date', 'type')


# class ItemViewSerializer(serializers.ModelSerializer):
#     product = ProductViewSerializer()

#     class Meta:
#         model = Item
#         fields = ('id', 'product', 'price', 'quantity')


# class ItemSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Item
#         fields = ('id', 'product', 'price', 'quantity', 'total')


# class EnquiryViewSerializer(serializers.ModelSerializer):
#     employee = EmployeeSerializer()
#     items = ItemViewSerializer(many=True, required=True)
#     activities = ActivityViewSerializer(many=True, required=True)

#     class Meta:
#         model = Enquiry
#         fields = ('id', 'type', 'status', 'priority', 'note', 'reason_for_lost', 'follow_up_date', 'contact', 'items',
#                   'amount', 'activities', 'updated_at', 'created_at', 'employee')


# class EnquirySerializer(serializers.ModelSerializer):
#     items = ItemSerializer(many=True, required=True)
#     activities = ActivitySerializer(many=True, required=True)
#     contact = serializers.PrimaryKeyRelatedField(queryset=Contact.objects.all(), required=True)

#     class Meta:
#         model = Enquiry
#         fields = ('id', 'type', 'status', 'priority', 'note', 'contact',
#                   'items', 'amount', 'activities', 'reason_for_lost', 'employee', 'follow_up_date',)

#     @transaction.atomic
#     def create(self, validated_data):
#         items = validated_data.pop('items')
#         activities = validated_data.pop('activities')
#         enquiry = Enquiry.objects.create(**validated_data)
#         for item in items:
#             Item.objects.create(enquiry=enquiry, **item)

#         Activity.objects.create(enquiry=enquiry, contact=enquiry.contact, employee=enquiry.employee, **activities[0])
#         return enquiry

#     @transaction.atomic
#     def update(self, enquiry, validated_data):
#             # reason_for_lost = validated_data.get('reason_for_lost', None)
#             # if reason_for_lost:
#             #     validated_data['status'] = Enquiry.STATUSES[2][0]

#             # enquiry.update(**validated_data)
#             # return enquiry
#         items_data = self.initial_data.pop('items')
#         activities_data = self.initial_data.pop('activities')
#         items_id_list = enquiry.items.values_list('id')
#         activity_id_list = enquiry.activities.values_list('id')

#         validated_data.pop('items')

#         for item_data in items_data:

#             if 'id' in item_data:
#                 id = item_data.pop('id')

#                 if id in items_id_list:
#                     items_id_list.remove(id)
#                     enquiry_item = Item.objects.get(id=id).update(**item_data)

#         Item.objects.filter(id__in=items_id_list).delete()
#         validated_data.pop('activities')

#         for activity_data in activities_data:

#             if 'id' in activity_data:
#                 id = activity_data.pop('id')

#                 if id in activity_id_list:
#                     activity_id_list.remove(id)
#                     activity_item = Activity.objects.get(id=id).update(**item_data)

#         Activity.objects.filter(id__in=activity_id_list).delete()

#         for item_data in items_data:
#             if 'id' not in item_data:
#                 product = Product.objects.get(id=item_data.pop('product'))
#                 Item.objects.create(enquiry=enquiry, product=product, **item_data)

#         for activity_data in activities_data:
#             if 'id' not in activity_data:
#                 type = ActivityType.objects.get(id=activity_data .pop('type'))
#                 Activity.objects.create(enquiry=enquiry, type=type, **activity_data)

#         enquiry.update(**validated_data)

#         return enquiry

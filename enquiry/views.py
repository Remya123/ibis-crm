from django.db import transaction
from django.contrib.auth.decorators import login_required, permission_required
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.forms import inlineformset_factory
from django.core.exceptions import PermissionDenied

from enquiry.models import Activity, Enquiry, CodeSetting
from enquiry.forms import EnquiryActivityForm, CodeSettingsForm,ActivityForm
from erp_core.pagination import paginate
import datetime

# from todo.models import Todo
# from timesheet.models import TimeSheet, TimeSheetEntry
# from calender.models import Holidays
# from globalconfig.globalconfig import isTimesheet

# TODO: block editing of send quote


@login_required
@transaction.atomic
# @permission_required('enquiry.add_activity', raise_exception=True)
def create_activity(request, enquiry_pk):

    enquiry = get_object_or_404(Enquiry, id=enquiry_pk)
    activity_type_form = ActivityForm()

    activity_form = EnquiryActivityForm(request.POST or None)
    if request.method == "POST":
        # date_str = datetime.datetime.strptime(request.POST.get('date'), "%d-%m-%Y %H:%M")
        # date = date_str.strftime("%Y-%m-%d %H:%M")

        # post = request.POST.copy()
        # # print("get...",post)
        # # import pdb; pdb.set_trace()

        # post.pop('date')
        # # print(request.POST)

        # request.POST = post
        # request.POST.update({'date': date})

        # print(request.POST)
        # import pdb; pdb.set_trace()

        if activity_form.is_valid():
            activity = activity_form.save(commit=False)
            activity.enquiry = enquiry
            activity.save()
            messages.add_message(request, messages.SUCCESS, 'Follow up created successfully')


            # description_id = request.POST.get('description')
            # description = ActivityDescription.objects.get(id=description_id)
            # activity = Activity.objects.create(date=date, description=description, enquiry=enquiry, contact=enquiry.contact, employee=enquiry.employee)
            # activity = activity_form.save(commit=False)
            # activity.enquiry = enquiry
            # activity.contact = enquiry.contact
            # activity.employee = enquiry.employee
            # activity.save()

            # todo = Todo.objects.create(name=activity.description + " (" + activity.type.name + " )",
            #                            employee=request.user.employee, deadline=activity.date, created_by=request.user.employee)

            # if isTimesheet():
            #     description = str(activity.type) + " " + str(enquiry.contact) + " for enquiry code " + str(enquiry.code)

            #     timesheet, status = TimeSheet.objects.get_or_create(
            #         employee=request.user.employee, approved_by=request.user.employee.reporting_officer, date=activity.date, sign_in_time=request.user.last_login)
            #     try:
            #         Holidays.objects.get(date=activity.date)
            #         timesheet.holiday = True
            #         timesheet.save()
            #     except Holidays.DoesNotExist:
            #         pass

            #     entry = TimeSheetEntry.objects.create(
            #         timesheet=timesheet, description=description, from_time=activity.date.time())
            # messages.add_message(request, messages.INFO,
            #                      'Sucessfully Timesheet created')

            return redirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

    context = {
        'enquiry': enquiry,
        'activity_form': activity_form,
        'form_url': reverse_lazy('enquiry:add_activity', args=[enquiry.id]),
        'type': 'Add',
        'activity_type_form':activity_type_form
    }
    return render(request, "activity/edit.html", context)


@login_required
@transaction.atomic
@permission_required('enquiry.change_activity', raise_exception=True)
def edit_activity(request, pk):
    activity = get_object_or_404(Activity, id=pk)
    enquiry = activity.enquiry
    activity_form = EnquiryActivityForm(request.POST or None, instance=activity)

    if request.method == "POST":
        # date_str = datetime.datetime.strptime(request.POST.get('date'), "%d-%m-%Y %H:%M")
        # date = date_str.strftime("%Y-%m-%d %H:%M")

        # post = request.POST.copy()
        # post.pop('date')

        # request.POST = post
        # request.POST.update({'date': date})

        if activity_form.is_valid():
            # description_id = request.POST.get('description')
            # description = ActivityDescription.objects.get(id=description_id)
            # # save sales order
            # activity.date = date
            # activity.description = description

            # activity = activity_form.save(commit=False)
            activity.save()
            messages.add_message(request, messages.SUCCESS, 'Follow up updated successfully')


            return redirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

    context = {
        'enquiry': enquiry,
        'activity_form': activity_form,
        'form_url': reverse_lazy('enquiry:edit_activity', args=[activity.id]),
        'type': 'Add',
    }
    return render(request, "activity/edit.html", context)


@login_required
@transaction.atomic
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        # if int(request.POST['count_index']) == 0:
        #     start_count = 0
        # else:
        #     start_count = int(request.POST['count_index']) - 1

        if code_form.is_valid():
            code_form.save()
            # code_settings.count_index =
            # code_settings.save()
            return HttpResponseRedirect(reverse('enquiry:list'))

    context = {

        'code_form': code_form,
    }

    return render(request, "enquiry/code_setting.html", context)


def delete_activity(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    activity.delete()
    messages.add_message(request, messages.SUCCESS, 'Follow up deleted successfully')

    return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[activity.enquiry.pk]))

def view_activity(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    context = {
    'activity':activity,
    }
    return render(request, "activity/detail.html", context)

def create_activity_type(request,pk):

    if request.method == "POST":
        activity_type_form = ActivityForm(request.POST)
        activity_type_form.save()
        return HttpResponseRedirect(reverse('enquiry:add_activity', args=[pk]))





# from django.contrib.auth.models import User
# from django.shortcuts import get_object_or_404
# from enquiry.serializers import ActivitySerializer, ActivityTypeSerializer, ActivityAppendSerializer
# from rest_framework import viewsets
# from rest_framework.response import Response
# from enquiry.models import Enquiry, Activity, ActivityType
# from rest_framework.decorators import list_route, detail_route
# from rest_framework import status


# class ActivityViewSet(viewsets.ViewSet):
#     """
#     A simple ViewSet for listing or retrieving users.

#     """
#     queryset = Activity.objects.all()
#     serializer_class = ActivityAppendSerializer

#     def list(self, request):
#         queryset = Activity.objects.all()
#         serializer = ActivitySerializer(queryset, many=True)
#         return Response(serializer.data)

#     def retrieve(self, request, pk=None):
#         queryset = Activity.objects.all()
#         user = get_object_or_404(queryset, pk=pk)
#         serializer = ActivitySerializer(user)
#         return Response(serializer.data)

#     @detail_route(methods=['post'])
#     def add(self, request, pk=None):
#         enquiry = get_object_or_404(Enquiry, pk=pk)
        
#         serializer = ActivityAppendSerializer(data=request.data)
#         if serializer.is_valid():
#             activity = Activity.objects.create(enquiry=enquiry, contact=enquiry.contact,
#                                                employee=enquiry.employee, description=serializer.data['description'], date=serializer.data['date'],location=serializer.data['location'])
#             activity.type_id = serializer.data['type']
#             activity.save()

#             # user.save()
#             return Response({'status': 'activity created'})
#         else:
#             return Response(serializer.errors,
#                             status=status.HTTP_400_BAD_REQUEST)

#     @list_route()
#     def choices(self, request):
#         activity_types = ActivityTypeSerializer(ActivityType.objects.all(), many=True).data
#         context = {
#             "activity_types": activity_types,

#         }
#         return Response(context)

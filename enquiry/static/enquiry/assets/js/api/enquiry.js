import Vue from 'vue'
// import Cookies from 'js-cookie'
// var csrftoken = Cookies.get('csrftoken')

// import axios from 'axios'
// axios.defaults.headers.common['X-CSRFToken'] = csrftoken

export default {
	getEnquiries(url) {
		// setTimeout(() => cb(_products), 100)
		return Vue.http.get(url)
	},

	getEnquiryOptions() {
		// setTimeout(() => cb(_products), 100)
		return Vue.http.get('/api/enquiries/choices')
	},

	get(id) {
		return Vue.http.get('/api/enquiries/' + id + '/')
	},

	format_enquiry(enquiry) {
		return {
			'type': enquiry.type,
			'status': enquiry.status,
			'note': enquiry.note,
			'follow_up_date': enquiry.follow_up_date,
			'reason_for_lost': enquiry.reason_for_lost,
			'customer': enquiry.customer.id,
			'items': this.format_items(enquiry.items),
			'activities': enquiry.activities.length ? enquiry.activities : null,
			'amount': enquiry.amount
		}
	},

	format_items(items) {
		if (items.length == 0)
			return null
		var enquiry_items = items.map((item) => {
			let i = Object.assign({}, item)
			i['product'] = item.product.id
			return i
		})
		// return items
		return enquiry_items
	},

	post(url, enquiry) {
		console.log(enquiry)
		var data = this.format_enquiry(enquiry)
		return Vue.http.post(url, data)
	},

	delete(url) {
		return Vue.http.delete(url)
	}
	// buyProducts (products, cb, errorCb) {
	//   setTimeout(() => {
	//     // simulate random checkout failure.
	//     (Math.random() > 0.5 || navigator.userAgent.indexOf('PhantomJS') > -1)
	//       ? cb()
	//       : errorCb()
	//   }, 100)
	// }
}

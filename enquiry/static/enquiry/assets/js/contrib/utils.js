export default {
    isEmpty(obj) {
        var p;
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                return false;
            }
        }
        return true;
    },
};
from django.db import models, transaction
from django.core.validators import MinValueValidator
from django.shortcuts import get_object_or_404
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting
from employee.models import Employee
from product.models import Program
# from globalconfig.models import GlobalConfig
# from geoposition.fields import GeopositionField

from erp_core.models import ActivityType
# Create your models here.
# should include time?
# enquiry source(eg: newspaper, website, referral, facebook, email, campaign, linkedin, sales team)
# estimated revenue -> sales pipeline
# status Open(Active), Won, Lost


# class Status(ErpAbstractBaseModel):
#     name = models.CharField(max_length=50)
#     value = models.CharField(max_length=50, null=True, blank=True)

#     def __str__(self):
#         return str(self.name)


class Enquiry(ErpAbstractBaseModel):

    def newCode():
        return CodeSetting.new_code()

    # TYPE_HOT = 'hot'
    # TYPE_COLD = 'cold'
    # TYPE_HOSTILE = 'hostile'
    # TYPES = (
    #     (TYPE_HOT, "HOT"),
    #     (TYPE_COLD, "COLD"),
    #     (TYPE_HOSTILE, "HOSTILE"),
    # )

    # STATUS_OPEN = 'open'
    # STATUS_CLOSE = 'approved'
    # STATUS_REJECTED = 'rejected'
    # STATUS_SUCCESSFULLY_CLOSED = 'successfully closed'
    # STATUSES = (
    #     (STATUS_OPEN, "OPEN"),
    #     (STATUS_CLOSE, "APPROVED"),  # Can't add activity
    #     (STATUS_REJECTED, "REJECTED"),
    #     (STATUS_SUCCESSFULLY_CLOSED, "STATUS_SUCCESSFULLY_CLOSED"),

    # )
    code = models.CharField(max_length=20, unique=True, default=newCode)
    source_of_lead = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    employee = models.ForeignKey('employee.Employee', null=True, blank=False)
    name_of_candidate = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=20)
    location = models.ForeignKey('location.City', null=True, blank=True)
    name_of_college = models.CharField(max_length=100, null=True, blank=True)
    year_of_passout = models.IntegerField(null=True, blank=True)
    course = models.ForeignKey(Program,null=True,blank=True)

    # products = models.ManyToManyField('Item', blank=True, related_name="enquiries", related_query_name="enquiry")

    # products = models.ManyToManyField('product.Product', blank=True, related_name="enquiries", related_query_name="enquiry")
    # class Meta:
    # ordering = ('-updated_at',)
    class Meta:

        verbose_name = "Leads"

    def __str__(self):
        return str(self.name_of_candidate)

    def save(self, *args, **kwargs):

        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()

        return super(Enquiry, self).save(*args, **kwargs)

    def can_create_invoice(self):
        pass
        # if GlobalConfig.objects.exists():
        #     config = GlobalConfig.objects.first()
        #     if not config.invoice and not hasattr(self, 'invoice'):
        #         return True
        # return False


class Activity(ErpAbstractBaseModel):
    date = models.DateTimeField()
    enquiry = models.ForeignKey(Enquiry, on_delete=models.CASCADE, related_name="activities", null=True, blank=True)
    type = models.ForeignKey(ActivityType, on_delete=models.CASCADE,
                             related_name="enquiry_activities", null=True, blank=True)
    employee = models.ForeignKey(Employee, null=True, blank=True)
    next_follow_up_date = models.DateField(null=True, blank=True)
    remark = models.TextField()

    complete = models.BooleanField(default=False)

    def __str__(self):
        return str(self.type) + str(self.date)


class CodeSetting(BaseCodeSetting):
    pass

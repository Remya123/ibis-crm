# from django.db import transaction

# from rest_framework import serializers
# from enquiry.models import Enquiry, Item, Activity, ActivityType
# # from employee.serializers import EmployeeSerializer
# from customer.serializers import ContactViewSerializer
# from customer.models import Contact
# # from customer.models import Customer
# # from django.shortcuts import get_object_or_404
# from product.serializers import ProductViewSerializer
# from timesheet.models import TimeSheet, TimeSheetEntry
# from calender.models import Holidays
# from globalconfig.globalconfig import isTimesheet
# from product.models import Product


# class ActivityTypeSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = ActivityType
#         fields = ('id', 'name')


# class ActivityViewSerializer(serializers.ModelSerializer):
#     type = ActivityTypeSerializer()

#     class Meta:
#         model = Activity
#         fields = ('id', 'description', 'date', 'type')


# class ActivitySerializer(serializers.ModelSerializer):
#     # description = serializers.CharField(required=True)

#     class Meta:
#         model = Activity
#         fields = ('id', 'description', 'date', 'type')


# # class GeopostionSerializer(serializers.Serializer):
# #     latitude = serializers.DecimalField(max_digits=50, decimal_places=30)
# #     longitude = serializers.DecimalField(max_digits=50, decimal_places=30)

# class ActivityAppendSerializer(serializers.ModelSerializer):
#     type = serializers.UUIDField()
#     location = serializers.CharField(max_length=255)

#     class Meta:
#         model = Activity
#         fields = ('id', 'description', 'type', 'date', 'location')


# class ItemViewSerializer(serializers.ModelSerializer):
#     product = ProductViewSerializer()

#     class Meta:
#         model = Item
#         fields = ('id', 'product', 'price', 'quantity', 'total')


# class ItemSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Item
#         fields = ('id', 'product', 'price', 'quantity', 'total')


# class EnquiryViewSerializer(serializers.ModelSerializer):
#     contact = ContactViewSerializer()
#     items = ItemViewSerializer(many=True, required=True)
#     activities = ActivityViewSerializer(many=True, required=True)
#     # employee = EmployeeSerializer()

#     class Meta:
#         model = Enquiry
#         fields = ('id', 'type', 'code', 'status', 'priority', 'note', 'reason_for_lost', 'follow_up_date', 'contact', 'items',
#                   'amount', 'activities', 'updated_at', 'created_at', 'can_create_quote', 'can_create_invoice', 'employee')


# class EnquirySerializer(serializers.ModelSerializer):
#     items = ItemSerializer(many=True, required=True)
#     activities = ActivitySerializer(many=True, required=True)
#     contact = serializers.PrimaryKeyRelatedField(queryset=Contact.objects.all(), required=True)

#     class Meta:
#         model = Enquiry
#         fields = ('id', 'code', 'type', 'status', 'priority', 'note', 'contact',
#                   'items', 'amount', 'activities', 'reason_for_lost', 'employee')

#     @transaction.atomic
#     def create(self, validated_data):
#         items = validated_data.pop('items')
#         activities = validated_data.pop('activities')
#         enquiry = Enquiry.objects.create(**validated_data)

#         for item in items:
#             Item.objects.create(enquiry=enquiry, **item)

#         activity = Activity.objects.create(enquiry=enquiry, contact=enquiry.contact,
#                                            employee=enquiry.employee, **activities[0])
#         if isTimesheet():
#             description = str(activity.type) + " " + str(enquiry.contact) + " for enquiry code " + str(enquiry.code)

#             timesheet, status = TimeSheet.objects.get_or_create(
#                 employee=enquiry.employee, approved_by=enquiry.employee.reporting_officer, date=activity.date, sign_in_time=enquiry.employee.user.last_login)
#             try:
#                 Holidays.objects.get(date=activity.date)
#                 timesheet.holiday = True
#                 timesheet.save()
#             except Holidays.DoesNotExist:
#                 pass
#             TimeSheetEntry.objects.create(timesheet=timesheet, description=description, from_time=activity.date.time())

#         return enquiry

#     @transaction.atomic
#     def update(self, enquiry, validated_data):
#         reason_for_lost = validated_data.get('reason_for_lost', None)
#         if reason_for_lost:
#             validated_data['status'] = Enquiry.STATUSES[2][0]

#         enquiry.update(**validated_data)
#         return enquiry
#         # items_data = self.initial_data.pop('items')
#         # activities_data = self.initial_data.pop('activities')
#         # items_id_list = enquiry.items.values_list('id')
#         # activity_id_list = enquiry.activities.values_list('id')
#         # validated_data.pop('items')
#         # validated_data.pop('activities')
#         # for item_data in items_data:

#         #     if 'id' in item_data:
#         #         id = item_data.pop('id')

#         #         if id in items_id_list:
#         #             items_id_list.remove(id)
#         #             enquiry_item = Item.objects.get(id=id).update(**item_data)

#         #     Item.objects.filter(id__in=items_id_list).delete()

#         # Item.objects.filter(id__in=items_id_list).delete()

#         # for activity_data in activities_data:

#         #     if 'id' in activity_data:
#         #         id = item_data.pop('id')

#         #         if id in activity_id_list:
#         #             activity_id_list.remove(id)
#         #             activity_item = Item.objects.get(id=id).update(**item_data)

#         # Item.objects.filter(id__in=activity_id_list).delete()

#         # for item_data in items_data:
#         #     if 'id' not in item_data:
#         #         product = Product.objects.get(id=item_data.pop('product'))
#         #         Item.objects.create(enquiry=enquiry, product=product, **item_data)

#         # for activity_data in activities_data:
#         #     if 'id' not in activity_data:
#         #         product = Product.objects.get(id=item_data.pop('product'))
#         #         Item.objects.create(enquiry=enquiry, product=product, **item_data)

#         # enquiry.update(**validated_data)

#         # return enquiry

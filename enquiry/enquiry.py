from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Prefetch, Q
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.db import IntegrityError, transaction
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError, EmailMessage, EmailMultiAlternatives,EmailMessage
from django.forms import inlineformset_factory, modelform_factory
from django import template
from django.template.loader import get_template
from django.template import Context
from django.core.exceptions import PermissionDenied
from enquiry.filters import EnquiryFilter
from enquiry.models import Enquiry, Activity
from enquiry.forms import EnquiryForm,EnquiryActivityForm, EnquiryEditForm
from employee.models import Employee
from erp_core.pagination import paginate
register = template.Library()
import socket
from smtplib import SMTPException
from location.forms import CityForm




@login_required
# @permission_required('enquiry.add_enquiry', raise_exception=True)
def list(request):
    from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger

    enquires = Enquiry.objects.order_by('-created_at')
    

    page_number = request.GET.get('page',1)

    f = EnquiryFilter(request.GET, queryset=enquires)

    paginator = Paginator(f.qs,20)


    # import pdb; pdb.set_trace()
    try:
        enquiries = paginator.page(page_number)
    except PageNotAnInteger:
        enquiries = paginator.page(1)
    except EmptyPage:
        enquiries = paginator.page(paginator.num_pages)

    # enquiries, pagination = paginate(request, queryset=f.qs, per_page=5, default=True)
    context = {
        'enquiries': enquiries,
        # 'pagination': pagination,
        'filter': f,
    }

    return render(request, "enquiry/list.html", context)


@login_required
def delete(request, pk):
    if request.method == "POST":
        enquiry = get_object_or_404(Enquiry, id=pk)
        enquiry.delete()
        messages.add_message(request, messages.SUCCESS, 'Lead deleted successfully')

    return HttpResponseRedirect(reverse('enquiry:list'))


@login_required
@transaction.atomic
def create(request):
    enquiry_form = EnquiryForm(request.POST or None)
    city_form = CityForm()
    if request.method == "POST":
        print (enquiry_form.errors)
        if enquiry_form.is_valid():
            enquiry = enquiry_form.save(commit=False)
            enquiry.employee=request.user.employee
            enquiry.save()
            messages.add_message(request, messages.SUCCESS, 'Lead created successfully')

            return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

    context = {
        'enquiry_form': enquiry_form,
        'form_url': reverse_lazy('enquiry:create'),
        'type': "Create",
        'city_form':city_form,


    }
    return render(request, "enquiry/edit.html", context)


@login_required
def enquiry_detail(request, pk):
    enquiry = get_object_or_404(Enquiry, id=pk)
    print (enquiry.activities.all())
    
    context = {
        'enquiry': enquiry,
        

        # 'hasInvoice': hasattr(quotation, "invoice"),

    }
    return render(request, "enquiry/enquiry_detail.html", context)



@login_required
@transaction.atomic
# @permission_required('enquiry.change_enquiry', raise_exception=True)
def edit(request, pk):
    enquiry = get_object_or_404(Enquiry, id=pk)
    enquiry_form = EnquiryForm(request.POST or None, instance=enquiry)
    if request.method == "POST":
        if enquiry_form.is_valid():
            enquiry = enquiry_form.save(commit=False)
            enquiry.save()
            messages.add_message(request, messages.SUCCESS, 'Lead updated successfully')

            return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

    context = {
        'enquiry': enquiry,
        'enquiry_form': enquiry_form,
        'form_url': reverse_lazy('enquiry:edit', args=[enquiry.id]),
        'type': "Edit",

    }
    return render(request, "enquiry/edit.html", context)


@login_required
def complete_activity(request, pk):
    activity = Activity.objects.get(id=pk)
    activity.complete = True
    activity.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def notification_mail(request, email, enquiry):
    subject = "Eazy link"
    # html_content = "<p>Hi " + enquiry.customer.user.first_name + ",</p><p>Your opportunity is " + enquiry.status.name + ".</p>"
    

    html_content = "<p>Hi "+enquiry.customer.user.first_name+" ,</p><p>Greetings from Eazylink.We are happy to help you find your right career.</p><p> Your application status : "+enquiry.status.name +"</p><p>.For further enquiries please contact us.</p><p>Thank You</p>"

    try:
        msg = EmailMessage(subject, html_content, 'infoeazylink@gmail.com',
                           [email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
        pass
    except BadHeaderError:
        # pass
        messages.add_message(request, messages.ERROR, 'Network Issue..')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))

    except socket.gaierror:
        # pass
        messages.add_message(request, messages.ERROR, 'Mail not send due to bad connection')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))

    except SMTPException:  # Didn't make an instance.
        # pass
        messages.add_message(request, messages.ERROR, 'Mail not send due to bad connection')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))

    except socket.error:
        # pass
        messages.add_message(request, messages.ERROR, 'Mail not send due to bad connection')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))


# @permission_required('enquiry.edit_enquiry_status', raise_exception=True)
def edit_status(request, pk):
    enquiry = get_object_or_404(Enquiry, id=pk)
    print ("jkk")

    if request.method == "POST":
        status_id = request.POST.get('status')
        status = get_object_or_404(Status, pk=status_id)

        enquiry.status = status
        enquiry.save()

        if enquiry.status.value == "SUCCESSFULLY_CLOSED":
            enquiry.successfully_closed_date = timezone.now().date()
            enquiry.save()
        # print (enquiry.customer.user.username)
        # import pdb; pdb.set_trace()
        notification_mail(request, enquiry.customer.user.username, enquiry)
        messages.add_message(request, messages.SUCCESS, 'Status Edited Successfully')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))


# @permission_required('enquiry.add_remarks', raise_exception=True)
def add_remark(request, pk):
    if request.method == "POST":
        enquiry = get_object_or_404(Enquiry, id=pk)

        remark_form = RemarkForm(request.POST or None, instance=enquiry)
        if remark_form.is_valid():
            remark_form.save()
        messages.add_message(request, messages.SUCCESS, 'Your Remarks Added')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))


# @permission_required('enquiry.add_remarks', raise_exception=True)
def add_onhold_remark(request, pk):
    if request.method == "POST":
        enquiry = get_object_or_404(Enquiry, id=pk)

        remark_form = OnholdRemarkForm(request.POST or None, instance=enquiry)
        if remark_form.is_valid():
            remark_form.save()
        messages.add_message(request, messages.SUCCESS, 'Your Remarks Added')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))


@login_required
def filter_based_on_office(request):
    # print("Start........")
    offices = Office.objects.all()
    crnt_date_for_check = ""
    if request.method == "POST":
        current_office = request.POST.get('office')
        date_check = request.POST.get('month_year')
        
        if date_check:
            date_for_check = datetime.datetime.strptime(date_check, "%B %Y").date()
            crnt_date_for_check = date_check

        else:
            date_for_check = datetime.date.today()

            tdate = datetime.date.today()
            crnt_date_for_check = datetime.datetime.strptime(str(tdate), '%Y-%m-%d').strftime('%B %Y')

        if current_office and date_for_check :
            enquires = Enquiry.objects.filter(employee__primary_designation__office__id=current_office,created_at__month=date_for_check.month,
        created_at__year=date_for_check.year)
            count=enquires.count()
        office_ob = Office.objects.get(pk=current_office)
        enquires, pagination = paginate(request, queryset=enquires, per_page=25, default=True)

        context = {
            'current_office': current_office,
            'enquires': enquires,
            'offices': offices,
            'date_for_check': crnt_date_for_check,
            'count': count,
            'office_ob':office_ob,
            'pagination':pagination,
        }
    else:
        context = {
            'offices': offices,
        }

    return render(request, "enquiry/filter_based_on_office.html", context)


@login_required
def stream_wise_report(request):
    # print("Start........")
    streams =  AreaOfInterest.objects.all()
    # offices = Office.objects.all()
    crnt_date_for_check = ""
    if request.method == "POST":
        current_stream = request.POST.get('stream')
        date_check = request.POST.get('month_year')
        
        if date_check:
            date_for_check = datetime.datetime.strptime(date_check, "%B %Y").date()
            crnt_date_for_check = date_check
        else:
            date_for_check = datetime.date.today()
            tdate = datetime.date.today()
            crnt_date_for_check = datetime.datetime.strptime(str(tdate), '%Y-%m-%d').strftime('%B %Y')

        if current_stream and date_for_check :
            products = Product.objects.filter(attributes__area_of_interest=current_stream)
            items = Item.objects.filter(product__id__in=[product.id for product in products])
            enquires = Enquiry.objects.filter(id__in=[item.enquiry.id for item in items],created_at__month=date_for_check.month,
        created_at__year=date_for_check.year)
            count=enquires.count()
        stream_ob = AreaOfInterest.objects.get(pk=current_stream)
        enquires, pagination = paginate(request, queryset=enquires, per_page=25, default=True)

        context = {
            'current_stream': current_stream,
            'enquires': enquires,
            'streams': streams,
            'date_for_check': crnt_date_for_check,
            'count': count,
            'stream_ob':stream_ob,
            'pagination':pagination
        }
    else:
        context = {
            'streams': streams,
        }

    return render(request, "enquiry/stream_wise_report.html", context)


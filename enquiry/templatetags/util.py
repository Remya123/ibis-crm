from django import template

register = template.Library()


@register.filter
def get_type(value):
    return type(value)


@register.simple_tag
def compare_uuid(arg1, arg2):
    if str(arg1) == str(arg2):
        return "selected"
    return ""


@register.simple_tag
def changeSign(arg1):

    return -arg1

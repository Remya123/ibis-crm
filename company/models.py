import uuid

from django.contrib import auth
from django.contrib.auth.models import Permission
from django.db import models
from smart_selects.db_fields import ChainedForeignKey

from erp_core.models import ErpAbstractBaseModel


class Division(ErpAbstractBaseModel):

    name = models.CharField(max_length=100)

    department = models.ForeignKey('Department', on_delete=models.CASCADE)

    division = models.ForeignKey('self', on_delete=models.CASCADE, related_name="parent_division", null=True,
                                 blank=True)

    def __str__(self):
        return str(self.name)


class Department(ErpAbstractBaseModel):

    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class Grade(ErpAbstractBaseModel):

    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class Designation(ErpAbstractBaseModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=100)
    minimum_working_hours = models.IntegerField(null=True, blank=True)

    zone = models.ForeignKey('location.Zone', on_delete=models.CASCADE, null=True, blank=True)

    location = models.ForeignKey('location.Location', on_delete=models.CASCADE, null=True, blank=True)

    office = ChainedForeignKey('location.Office',
                               chained_field="location",
                               chained_model_field="location",
                               show_all=False,
                               auto_choose=True,
                               sort=True, on_delete=models.CASCADE, null=True, blank=True)

    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)

    division = ChainedForeignKey(Division,
                                 chained_field="department",
                                 chained_model_field="department",
                                 show_all=False,
                                 auto_choose=True,
                                 sort=True, on_delete=models.CASCADE, null=True, blank=True)

    grade = models.ForeignKey(Grade, on_delete=models.CASCADE, null=True, blank=True)
    permissions = models.ManyToManyField(Permission, blank=True)

    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        # auth.models.Group.objects.create(name=self.name)
        super(Designation, self).save(*args, **kwargs)


# class BasicPayDaily(ErpAbstractBaseModel):
#     designation = models.ForeignKey(Designation, on_delete=models.CASCADE)
#     grade = models.ForeignKey(Grade, on_delete=models.CASCADE)
#     amount = models.DecimalField(max_digits=20, decimal_places=2)
#
#     def __str__(self):
#         return str(self.designation)
# # Create your models here.
#
#
# class ModeOfDelivery(ErpAbstractBaseModel):
#     TYPES = (
#             ('self', "SELF"),
#             ('3PL', "3PL"),
#             ('dispatch', "DISPATCH"),
#     )
#
#     type = models.CharField(max_length=30, choices=TYPES, default=TYPES[0])
#
#     def __str__(self):
#         return self.type

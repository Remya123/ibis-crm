from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.

        item(
            'Programs',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",

            children=[
                # item(
                #     'Add Program',
                #     'product:add_products',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),
                # item(
                #     'Program List',
                #     'product:view_products',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),
                # #  item(
                #     'Add HSN',
                #     'admin:gst_hsn_changelist',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),



            ]

        )

    ]),

    # tree('customer_sidebar', items=[
    #     # Then define items and their children with `item` function.

    #     item(
    #         'Programs',
    #         'product:view_products',
    #         in_menu=True,
    #         in_sitetree=True,
    #         access_loggedin=True,
    #         icon_class="clipboard",

    #         children=[
    #             # item(
    #             #     'Add Program',
    #             #     'product:add_products',
    #             #     in_menu=True,
    #             #     in_sitetree=True,
    #             #     access_loggedin=True,
    #             #     icon_class="print",
    #             #     # access_by_perms=['enquiry.view_quote_list'],

    #             # ),
    #             # item(
    #             #     'Program List',
    #             #     'product:view_products',
    #             #     in_menu=True,
    #             #     in_sitetree=True,
    #             #     access_loggedin=True,
    #             #     icon_class="print",
    #             #     # access_by_perms=['enquiry.view_quote_list'],

    #             # ),
    #             # #  item(
    #             #     'Add HSN',
    #             #     'admin:gst_hsn_changelist',
    #             #     in_menu=True,
    #             #     in_sitetree=True,
    #             #     access_loggedin=True,
    #             #     icon_class="print",
    #             #     # access_by_perms=['enquiry.view_quote_list'],

    #             # ),



    #         ]

    #     )

    # ]),
    # ... You can define more than one tree for your app.
)

from django.db import models
from erp_core.models import BaseModel, BaseCodeSetting
from erp_core.models import ErpAbstractBaseModel
from decimal import Decimal
# from inventory.models import Batch
import uuid
from django.core.validators import MinValueValidator


class Program(ErpAbstractBaseModel):
    name = models.CharField(max_length=255)
    accademic_eligibility = models.CharField(max_length=255, null=True, blank=True)
    total_fee = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return str(self.name)


# class University(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255, unique=True)
#     country = models.ForeignKey(Country)

#     def __str__(self):
#         return str(self.name) + " - " + str(self.country)


# class RawMaterialManager(models.Manager):
#     def get_queryset(self):
#         return super(RawMaterialManager, self).get_queryset().filter(raw_material=True)


# class Product(ErpAbstractBaseModel):
#     objects = models.Manager()
#     raw_materials = RawMaterialManager()

#     def newCode():
#         return CodeSetting.new_code()

#     name = models.CharField(max_length=255)
#     code = models.CharField(max_length=30, unique=True, default=newCode)
#     # batch = models.ForeignKey(Batch, on_delete=models.CASCADE, null=True, blank=True, related_name="%(class)ss")
#     price = models.DecimalField("Total Fee in INR", max_digits=20, decimal_places=2,
#                                 null=True, blank=True, validators=[MinValueValidator(0)])
#     # cost_price = models.DecimalField(max_digits=20, decimal_places=2, null=True,
#     #                                  blank=True, validators=[MinValueValidator(0)])
#     belongs_to = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name="contains")
#     category = models.ForeignKey(Category, null=True, blank=True,
#                                  related_name="program_level", verbose_name="Levels of course")
#     # +taxes = models.ManyToManyField(Tax)
#     active = models.BooleanField(default=True)
#     raw_material = models.BooleanField(default=False)
#     # hsn = models.ForeignKey(Hsn, null=True, blank=True)

#     def __str__(self):
#         return self.name + '-' + str(self.attributes.university)

#     def save(self, *args, **kwargs):
#         if self.code and self.created_at is None:
#             CodeSetting.incrementCountIndex()

#         return super(Product, self).save(*args, **kwargs)
#     # def priceExclusiveOfTaxPrice(self):
#     #     return self.price * (1 - taxes / 100)

#     def totalTaxRate(self):
#         taxes = self.taxes.aggregate(total_tax=models.Sum('rate'))['total_tax']
#         if taxes:
#             return Decimal(taxes)
#         else:
#             return 0

#     class Meta:
#         verbose_name_plural = "Program"
#         verbose_name = "Programs"


# class AttributeDecimalField(models.DecimalField):

#     def __init__(self, *args, **kwargs):
#         kwargs['max_digits'] = 10
#         kwargs['decimal_places'] = 2
#         kwargs['null'] = True
#         kwargs['blank'] = True
#         super(AttributeDecimalField, self).__init__(*args, **kwargs)


# class Attribute(ErpAbstractBaseModel):
#     DISTANCE_UNITS = (
#         ('m', 'meter'),
#         ('in', 'inch'),
#         ('ft', 'foot'),
#         ('cm', 'centimeter'),
#         ('mm', 'millimeter'),
#     )

#     MASS_UNITS = (
#         ('kg', 'kilogram'),
#         ('mg', 'milligram'),
#         ('g', 'gram'),
#         ('cm', 'centimeter'),
#         ('mm', 'millimeter'),
#     )

#     VOLUME_UNITS = (
#         ('cc', 'cubic centimeter'),
#         ('L', 'liter'),
#     )

#     AREA_UNITS = (
#         ('m2', 'square meters'),
#         ('cm2', 'square centimeter'),
#         ('ft2', 'square feet'),
#     )
#     # unit_of_length = models.CharField(max_length=30, choices=DISTANCE_UNITS,
#     #                                   default=DISTANCE_UNITS[0], blank=True, null=True)
#     # length = AttributeDecimalField(blank=True, null=True)
#     # breadth = AttributeDecimalField(blank=True, null=True)
#     # height = AttributeDecimalField(blank=True, null=True)

#     # unit_of_mass = models.CharField(max_length=30, choices=MASS_UNITS, default=MASS_UNITS[0], blank=True, null=True)
#     # weight = AttributeDecimalField(blank=True, null=True)

#     # unit_of_volume = models.CharField(max_length=30, choices=VOLUME_UNITS,
#     #                                   default=VOLUME_UNITS[0], blank=True, null=True)
#     # volume = AttributeDecimalField(blank=True, null=True)

#     # unit_of_area = models.CharField(max_length=30, choices=AREA_UNITS, default=AREA_UNITS[0], blank=True, null=True)
#     # area = AttributeDecimalField(blank=True, null=True)

#     # ingredients = models.TextField(blank=True, null=True)
#     # supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE, default=uuid.uuid4, related_name="attributes")

#     product = models.OneToOneField(Product, on_delete=models.CASCADE, related_name="attributes")
#     area_of_interest = models.ForeignKey(AreaOfInterest, null=True, blank=True)
#     # country = models.ForeignKey(Country, null=True, blank=True)
#     university = models.ForeignKey(University, null=True, blank=True)
#     course_details = models.FileField(upload_to='course/%Y/%m/%d', blank=True, null=True)
#     accademic_eligibility = models.ForeignKey(Qualification, null=True, blank=True)
#     min_Percentage_requried = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
#     scholarship_options = models.CharField(max_length=255, null=True, blank=True)
#     criteria_for_scholarship = models.CharField(max_length=255, null=True, blank=True)
#     no_of_arrears_allowed = models.IntegerField(null=True, blank=True)
#     # ielts_score_requirement = models.IntegerField(null=True, blank=True)
#     # gmat_sat_gre_score = models.IntegerField(null=True, blank=True)
#     other_interance_exams = models.CharField(max_length=255, null=True, blank=True)
#     stay_back_availability_in_months = models.IntegerField(null=True, blank=True)
#     career_possibilities = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)

#     def __str__(self):
#         return str(self.product)


class CodeSetting(BaseCodeSetting):
    pass


# class ProductLanguageAssesement(ErpAbstractBaseModel):
#     product = models.ForeignKey(Product, related_name="prod_languages")
#     language_assesement = models.ForeignKey(
#         LanguageAssessmentTest, related_name="prod_languages", null=True, blank=True)
#     score = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)

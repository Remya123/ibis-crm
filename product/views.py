from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F, Q, functions, Value as V
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render, redirect
from erp_core.pagination import paginate
import pprint
from .filters import ProductFilter
from django.forms import inlineformset_factory
from django.db import transaction
from product.models import *
# Create your views here.


@login_required
def add_products(request):
    form = ProductsForm(request.POST or None)

    if request.method == "POST":
        # print(form.errors)
        # print(form2.errors)

        if form.is_valid():
            product = form.save(commit=False)
            product.save()
            messages.add_message(request, messages.INFO, 'Program created sucessfully')
            return HttpResponseRedirect(reverse('product:detail', args=[product.id]))
    context = {
        "form": form,
        "type": "add",
    }
    return render(request, 'product/add_products.html', context)

# Details the Products


@login_required
def view_products(request):
    from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
    page_number = request.GET.get('page', 1)

    f = ProductFilter(request.GET, queryset=Program.objects.all().order_by("-created_at"))
    paginator = Paginator(f.qs, 20)

    # import pdb; pdb.set_trace()
    try:
        products = paginator.page(page_number)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    # products = Product.objects.all().order_by("-created_at")
    # products, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    # upload_form = UploadFileForm()

    # products = Product.objects.all().order_by("-created_at")
    # products, pagination = paginate(request, queryset=products, per_page=10, default=True)
    # for page in pagination:
    #     print(page)
    context = {
        "products": products,
        # 'pagination': pagination,
        'filter': f,
        # 'upload_form': upload_form,

    }
    return render(request, 'product/view_products.html', context)


@login_required
def detail(request, pk):
    product = Program.objects.get(id=pk)
    context = {
        "product": product,
        "pk": pk
    }
    return render(request, "product/detail.html", context)


@login_required
def edit_product(request, product_pk):

    product = Program.objects.get(id=product_pk)
    form = ProductsForm(request.POST or None, instance=product)

    if request.method == "POST":
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('product:detail', args=[product.id]))
    context = {
        "form": form,
        "type": "edit",
    }
    return render(request, 'product/add_products.html', context)


@login_required
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            return HttpResponseRedirect(reverse('product:view_products'))

    context = {
        'code_form': code_form,
    }

    return render(request, "product/code_setting.html", context)


@login_required
def delete(request, pk):
    if request.method == 'POST':
        product = get_object_or_404(Product, pk=pk)
        product.delete()
        return HttpResponseRedirect(reverse('product:view_products'))


def dashboard(request):
    return render(request, "product/dashboard.html")


# @transaction.atomic
# @login_required
# # @permission_required('employee.add_employee', raise_exception=True)
# def import_data(request):
#     form = UploadFileForm(request.POST or None, request.FILES or None)
#     if request.method == "POST":
#         # form = UploadFileForm(request.POST, request.FILES)
#         # def user_func(row):
#         #     # print(row)
#         #     row[3] = make_password(row[3])
#         #     return row

#         # def prod_func(row):
#         #     # print(row)
#         #     user = User.objects.filter(username=row[0]).first()
#         #     row[0] = user
#         #     designation = Designation.objects.filter(name=row[1]).first()
#         #     row[1] = designation
#         #     return row

#         # def contact_func(row):
#         #     # print(row)
#         #     employee = Employee.objects.filter(code=row[0]).first()
#         #     row[0] = employee
#         #     return row

#         if form.is_valid():
#             file_data = request.FILES['file'].get_records()
#             # file_data = request.FILES['file']

#             # print(file_data)
#             # for row in file_data:
#             #     print(row['name'].first)
#             # import pdb
#             # pdb.set_trace()
#             # print(file_data)
#             # import pdb
#             # pdb.set_trace()
#             # for row in file_data:

#             #     if row['Levels Of Course'] is not None:
#             #         category = Category.objects.create(name=row['Levels Of Course'])

#             for row in file_data:
#                 # print(row)
#                 # user = User.objects.filter(username=row['username']).first()

#                 category = Category.objects.filter(name__icontains=row['Levels Of Course']).first()

#                 accademic_eligibility = Qualification.objects.filter(
#                     name__icontains=row['Accademic Eligibility']).first()

#                 area_of_interest = AreaOfInterest.objects.filter(
#                     name__icontains=row['Stream']).first()

#                 university = University.objects.filter(
#                     name__icontains=row['University']).first()
#                 product = Product.objects.create(
#                     name=row['Name'], code=row['Code'], price=row['Fee'], category=category, active=True)
#                 ielts = LanguageAssessmentTest.objects.filter(name="Ielts").first()
#                 gert = LanguageAssessmentTest.objects.filter(name="Gert").first()
#                 gort = LanguageAssessmentTest.objects.filter(name="Gort").first()
#                 # ContactForEmployee.objects.create(
#                 #     employee=employee, phone=row['phone'], alternate_phone=row['alternate_phone'])
#                 Attribute.objects.create(product=product, accademic_eligibility=accademic_eligibility, min_Percentage_requried=row['Minimum Percentage Required'], scholarship_options=row['Scholarship Options'], criteria_for_scholarship=row['Criteria For Scholarship'],
#                                          no_of_arrears_allowed=row['No Of Arrears Allowed'], other_interance_exams=row['Other Enterance Exams'],
#                                          stay_back_availability_in_months=row['Stay Back Availability in Months'], career_possibilities=row['Career Posibilities'], area_of_interest=area_of_interest, university=university)

#                 ProductLanguageAssesement.objects.bulk_create([
#                     ProductLanguageAssesement(product=product, language_assesement=ielts,
#                                               score=row['IELTS Score Required']),
#                     ProductLanguageAssesement(product=product, language_assesement=gert,
#                                               score=row['GERT Score Required']),
#                     ProductLanguageAssesement(product=product, language_assesement=gort,
#                                               score=row['GORT Score Required'])
#                 ])
#     return redirect("product:view_products")

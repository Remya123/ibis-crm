# from django.http import JsonResponse, HttpResponse
# from django.views.decorators.csrf import ensure_csrf_cookie
# from django.db.models import F
# from .serializers import ProductViewSerializer
# from product.models import Product
# from django.contrib.auth.decorators import login_required, permission_required
# from django.contrib.auth.models import Permission, User
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.renderers import JSONRenderer
# from rest_framework.parsers import JSONParser
# from rest_framework.decorators import list_route, api_view, permission_classes
# from rest_framework.response import Response
# from rest_framework import viewsets, generics, status
# from django.utils.decorators import method_decorator


# # import json


# @ensure_csrf_cookie
# def product_select_options(request):
#     name = request.GET.get('name', False)
#     if name:
#         products = Product.objects.filter(
#             name__contains=name
#         ).annotate(
#             text=F('name')
#         ).values('id', 'text')
#         return JsonResponse(list(products), safe=False)
#     return JsonResponse(list(), safe=False)


# # @ensure_csrf_cookie
# # def all_products(request):
# #     products = Product.objects.filter(active=True)
# #     serializer = ProductViewSerializer(products, many=True)
# #     return JsonResponse({'result': serializer.data}, safe=False)


# class ListModelMixin(object):
#     """
#     List a queryset.
#     """

#     def list(self, request, *args, **kwargs):
#         queryset = self.filter_queryset(self.get_queryset())

#         page = self.paginate_queryset(queryset)
#         if page is not None:
#             serializer = self.get_serializer(page, many=True)
#             return self.get_paginated_response(serializer.data)

#         serializer = self.get_serializer(queryset, many=True)
#         return Response(serializer.data)


# @method_decorator(ensure_csrf_cookie, name='dispatch')
# class ProductList(generics.ListAPIView, ListModelMixin):
#     serializer_class = ProductViewSerializer
#     renderer_classes = (JSONRenderer, )

#     def get_queryset(self):
#         products = Product.objects.filter(active=True)
#         return products

#     def list(self, request):
#         response = super(ProductList, self).list(request)
#         response.template_name = "product/all_products.html"
#         return response

from rest_framework import serializers
from product.models import Program
# from gst.serializers import HsnSerializer


class ProductViewSerializer(serializers.ModelSerializer):

    # hsn = HsnSerializer()

    class Meta:
        model = Program
        # fields = ('id', 'name', 'code', 'category', 'hsn')
        fields = ('id', 'name')

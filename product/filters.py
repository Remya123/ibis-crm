import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from product.models import Program


class ProductFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    accademic_eligibility = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    # price = django_filters.NumberFilter(widget=forms.TextInput(attrs={'class': 'form-control'}))
    # cost_price = django_filters.NumberFilter(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Program
        fields = ['name', ]

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)

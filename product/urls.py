from django.conf.urls import include, url
from product import api, views

app_name = "product"

# api_patterns = ([

#     # url(r'^invoices/$', api.invoices, name="invoices"),
#     url(r'^products/$', api.ProductList.as_view()),

#     # url(r'^invoice/(?P<code>[^/]+)/payments/$', api.payment_details, name="payment_details"),

# ], 'api')


urlpatterns = [
    # url(r'^api/', include([
    #     # router.urls,
    #     url(r'^products/', include([
    #         url(r'^select_list/$', api.product_select_options, name="select_list"),
    #     ])),
    #     # get active products -api
    #     # url(r'^products/$', api.all_products, name="all_products"),

    # ], namespace='api')),
    url(r'^programs/add_program/$', views.add_products, name='add_products'),
    url(r'^programs/listProgram/$', views.view_products, name='view_products'),
    url(r'^programs/(?P<pk>[^/]+)/detail/$', views.detail, name='detail'),
    url(r'^programs/edit_program/(?P<product_pk>[^/]+)/$',
        views.edit_product, name='edit_product'),
    url(r'^programs/code/settings/$', views.code_settings, name='code_settings'),
    url(r'^programs/(?P<pk>[^/]+)/delete/$', views.delete, name='delete'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    # url(r'^import/$', views.import_data, name="import"),


]

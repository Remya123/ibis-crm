from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Program
from .models import CodeSetting
# from .models import Stock


class ProductsForm(ModelForm):
    class Meta:
        model = Program

        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
            "accademic_eligibility": forms.TextInput(attrs={'class': "form-control1", 'required': "required"}),
            "total_fee": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),


        }
        fields = ['name', 'accademic_eligibility', 'total_fee']


# class AttributeForm(ModelForm):
#     class Meta:
#         model = Attribute

#         widgets = {

#             # "unit_of_length": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "length": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "breadth": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "height": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "unit_of_mass": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "weight": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "unit_of_volume": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "volume": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "unit_of_area": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "area": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "ingredients": forms.TextInput(attrs={'class': "form-control1"}),
#             # "supplier": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             "area_of_interest": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             "university": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             # "country": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             "course_details": forms.FileInput(attrs={'class': 'form-control'}),
#             "accademic_eligibility": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             "min_Percentage_requried": forms.NumberInput(attrs={'class': "form-control1"}),
#             "scholarship_options": forms.TextInput(attrs={'class': "form-control1"}),
#             "criteria_for_scholarship": forms.TextInput(attrs={'class': "form-control1"}),
#             "no_of_arrears_allowed": forms.NumberInput(attrs={'class': "form-control1"}),
#             "ielts_score_requirement": forms.NumberInput(attrs={'class': "form-control1"}),
#             "gmat_sat_gre_score": forms.NumberInput(attrs={'class': "form-control1"}),
#             "other_interance_exams": forms.TextInput(attrs={'class': "form-control1"}),
#             "stay_back_availability_in_months": forms.NumberInput(attrs={'class': "form-control1"}),
#             "career_possibilities": forms.NumberInput(attrs={'class': "form-control1"}),

#         }
#         # fields = ['unit_of_length', 'length', 'breadth', 'height', 'unit_of_mass', 'weight',
#         #           'unit_of_volume', 'volume', 'unit_of_area', 'area', 'ingredients', 'supplier']
#         fields = ['university', 'course_details', 'area_of_interest', 'accademic_eligibility', 'min_Percentage_requried', 'scholarship_options',
#                   'criteria_for_scholarship', 'no_of_arrears_allowed', 'other_interance_exams', 'stay_back_availability_in_months', 'career_possibilities']


class CodeSettingsForm(ModelForm):

    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


# class AttributeEditForm(ModelForm):
#     class Meta:
#         model = Attribute

#         widgets = {

#             # "unit_of_length": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "length": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "breadth": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "height": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "unit_of_mass": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "weight": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "unit_of_volume": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "volume": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "unit_of_area": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             # "area": forms.NumberInput(attrs={'class': "form-control1"}),
#             # "ingredients": forms.TextInput(attrs={'class': "form-control1"}),
#             # "supplier": forms.Select(attrs={'id': "select1", 'class': 'form-control1', "multiple": False}),
#             "area_of_interest": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             "university": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             # "country": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             "course_details": forms.FileInput(attrs={'class': 'form-control'}),
#             "accademic_eligibility": forms.Select(attrs={'class': 'form-control', 'required': "required"}),
#             "min_Percentage_requried": forms.NumberInput(attrs={'class': "form-control1"}),
#             "scholarship_options": forms.TextInput(attrs={'class': "form-control1"}),
#             "criteria_for_scholarship": forms.TextInput(attrs={'class': "form-control1"}),
#             "no_of_arrears_allowed": forms.NumberInput(attrs={'class': "form-control1"}),
#             "ielts_score_requirement": forms.NumberInput(attrs={'class': "form-control1"}),
#             "gmat_sat_gre_score": forms.NumberInput(attrs={'class': "form-control1"}),
#             "other_interance_exams": forms.TextInput(attrs={'class': "form-control1"}),
#             "stay_back_availability_in_months": forms.NumberInput(attrs={'class': "form-control1"}),
#             "career_possibilities": forms.NumberInput(attrs={'class': "form-control1"}),


#         }
#         # fields = ['unit_of_length', 'length', 'breadth', 'height', 'unit_of_mass', 'weight',
#         #           'unit_of_volume', 'volume', 'unit_of_area', 'area', 'ingredients', 'supplier']
#         fields = ['university', 'area_of_interest', 'accademic_eligibility', 'min_Percentage_requried', 'scholarship_options',
#                   'criteria_for_scholarship', 'no_of_arrears_allowed', 'other_interance_exams', 'stay_back_availability_in_months', 'career_possibilities']


# class ProductsLanguageForm(ModelForm):
#     class Meta:
#         model = ProductLanguageAssesement

#         widgets = {

#             "language_assesement": forms.Select(attrs={'class': "form-control1"}),
#             "score": forms.NumberInput(attrs={'class': "form-control1"}),


#         }
#         exclude = ['product', ]


# class UploadFileForm(forms.Form):
#     file = forms.FileField()

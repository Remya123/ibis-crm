from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from location.models import *
from student.models import Student, AddressForStudent, ContactForStudent
# from django.core.exceptions import NON_FIELD_ERRORS


class StudentForm(ModelForm):

  class Meta:
    model = Student
    widgets = {"code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
               "name_of_college": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "name_of_college"}),
               "name": forms.TextInput(attrs={'class': "form-control"}),
               "year_of_passout": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
               # "religion": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
               "photo": forms.FileInput(attrs={'class': "form-control"}),
               "location": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "course": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "gender": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "gender": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "campaign": forms.Select(attrs={'class': "form-control", "data-toggle": "select"}),

               }
    fields = ['code', 'name_of_college', 'name', 'year_of_passout', 'photo', 'location', 'course', 'gender', 'campaign']


class AddressForStudentForm(ModelForm):
  class Meta:
    model = AddressForStudent

    widgets = {"addressline1": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
               "addressline2": forms.TextInput(attrs={'class': "form-control"}),
               "area": forms.TextInput(attrs={'class': "form-control", }),
               "zipcode": forms.NumberInput(attrs={'class': "form-control", }),
               # "city": forms.Select(attrs={'class': "chained form-control", }),
               # "state": forms.Select(attrs={'class': "chained form-control", }),
               # "country": forms.Select(attrs={'class': "chained form-control",}),
               }
    fields = ['addressline1', 'addressline2', 'area', 'zipcode', 'city', 'state', 'country']


class ContactForStudentForm(ModelForm):
  class Meta:
    model = ContactForStudent

    widgets = {"phone": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
               "alternate_phone": forms.NumberInput(attrs={'class': "form-control", }),
               }
    fields = ['phone', 'alternate_phone']


class StudentEditForm(ModelForm):
  class Meta:
    model = Student

    widgets = {"code": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
               "name_of_college": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "name_of_college"}),
               "name": forms.TextInput(attrs={'class': "form-control"}),
               "year_of_passout": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
               # "religion": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
               "photo": forms.FileInput(attrs={'class': "form-control"}),
               "location": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "course": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "gender": forms.Select(attrs={'class': "form-control", 'required': "required"}),
               "campaign": forms.Select(attrs={'class': "form-control"}),

               }
    fields = ['code', 'name_of_college', 'name', 'year_of_passout', 'photo', 'location', 'course', 'gender', 'campaign']

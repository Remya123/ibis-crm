from django.contrib import admin
from .models import StudentCodeSetting, Student, AddressForStudent, ContactForStudent
# Register your models here.
admin.site.register(StudentCodeSetting)
admin.site.register(Student)
admin.site.register(AddressForStudent)
admin.site.register(ContactForStudent)

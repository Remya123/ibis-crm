from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F, Q, functions, Value as V
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from guardian.shortcuts import get_objects_for_user
from location.models import *
from django.contrib.auth.models import Group
import datetime
from django.db.models import Sum
from django.contrib import messages
from erp_core.pagination import paginate
from .models import *
from django.utils import timezone
from django.db import IntegrityError, transaction
from student.filters import StudentFilter


    
def create(request, pk):
    
    enquiry = get_object_or_404(Enquiry, pk=pk)
    form2 = StudentForm(request.POST or None, request.FILES or None,initial={'name':enquiry.name_of_candidate,'name_of_college':enquiry.name_of_college,'year_of_passout':enquiry.year_of_passout,'location':enquiry.location,'course':enquiry.course})
    form4 = ContactForStudentForm(request.POST or None,initial={'phone':enquiry.contact_number})
    form3 = AddressForStudentForm(request.POST or None)

    if request.method == "POST":

        if form2.is_valid() and form3.is_valid() and form4.is_valid():
            try:
                with transaction.atomic():
                    student = form2.save(commit=False)
                    student.enquiry = enquiry
                    student.save()
                    address = form3.save(commit=False)
                    address.student = student
                    address.save()
                    contact = form4.save(commit=False)
                    contact.student = student
                    contact.save()

                    messages.add_message(request, messages.SUCCESS, 'Student created Successfully.')
                    return HttpResponseRedirect(reverse('student:detail',args=[student.pk]))

            except IntegrityError:
                messages.add_message(request, messages.ERROR, 'Please try again.some network issues.')
            return HttpResponseRedirect(reverse('student:create'))

    context = {
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form_url': reverse_lazy('student:create',args=[pk])

    }

    return render(request, 'student/create.html', context)


@login_required
@permission_required('employee.change_employee', raise_exception=True)


def edit(request, pk):
    student = get_object_or_404(Student, code=pk)

    if request.method == "POST":
        form2 = StudentForm(request.POST, request.FILES, instance=student)
        form3 = AddressForStudentForm(request.POST, instance=student.student_address)
        form4 = ContactForStudentForm(request.POST, instance=student.student_contact)

        if form2.is_valid() and form3.is_valid() and form4.is_valid():
            student = form2.save()
            # student.user = user
            address = form3.save(commit=False)
            address.student = student
            address.save()
            contact = form4.save(commit=False)
            contact.student = student
            contact.save()
            return HttpResponseRedirect(reverse('student:detail',args=[student.pk]))

    else:

        form2 = StudentForm(instance=student)
        form3 = AddressForStudentForm(instance=student.student_address)
        form4 = ContactForStudentForm(instance=student.student_contact)

    context = {

        'form2': form2,
        'form3': form3,
        'form4': form4,
        'edit': "edit",
        'form_url': reverse_lazy('student:edit', args=[student.code])

    }

    return render(request, 'student/create.html', context)


# @login_required
# @permission_required('employee.delete_employee', raise_exception=True)
def delete(request, pk):
    if request.method == "POST":
        student = get_object_or_404(Student, code=pk)
        student.delete()
        return HttpResponseRedirect(reverse('student:list'))


# @login_required
# # @permission_required('employee.view_employee_details', raise_exception=True)
def detail(request, pk):
    student = get_object_or_404(Student,pk=pk)
    context = {
    'student': student,

    }
    return render(request, 'student/detail.html',context)

def index(request):
    students = Student.objects.all()
    f = StudentFilter(request.GET, queryset=students)
    students, pagination = paginate(request, queryset=f.qs, per_page=5, default=True)


    context = {
        'students': students,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, "student/index.html", context)
import django_filters
from django_filters import widgets
from django import forms
from django.db.models import F, Prefetch, functions, Q, Value as V
from student.models import Student
from location.models import City


class StudentFilter(django_filters.FilterSet):

    code = django_filters.CharFilter(name="code", lookup_expr='contains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(name="name", lookup_expr='contains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    # next_follow_up_date = django_filters.DateFilter(name="next_follow_up_date")
    # next_follow_up_date = django_filters.DateFilter(label="Next Follow Up Date", name="next_follow_up_date", widget=forms.DateInput(attrs={'class': 'form-control'}))
    # date = django_filters.DateFilter(label="Date", name="date", widget=forms.DateInput(attrs={'class': 'form-control'}))
    # type = django_filters.MultipleChoiceFilter(choices=Enquiry.TYPES)
    # priority = django_filters.MultipleChoiceFilter(choices=Enquiry.PRIORITIES)

    class Meta:
        model = Student
        fields = ['code', ]


# class WebEnquiryFilter(django_filters.FilterSet):
#     code = django_filters.CharFilter(lookup_expr='iexact', method='filter_by_code', widget=forms.TextInput(attrs={'class': 'form-control'}))

#     amount = django_filters.NumberFilter(widget=forms.Select(attrs={'class': 'form-control'}))

#     min_amount = django_filters.NumberFilter(name='amount', lookup_expr='gte',
#                                              widget=forms.Select(attrs={'class': 'form-control'}))
#     max_amount = django_filters.NumberFilter(name='amount', lookup_expr='lte',
#                                              widget=forms.Select(attrs={'class': 'form-control'}))

#     contact = django_filters.CharFilter(label="Contact", method='filter_by_contact',
#                                         widget=forms.TextInput(attrs={'class': 'form-control'}))

#     # status = django_filters.ChoiceFilter(choices=Enquiry.STATUSES, widget=forms.Select(attrs={'class': 'form-control'}))
#     status = django_filters.ModelChoiceFilter(
#         queryset=Status.objects.all(),
#         method='filter_by_status',
#         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
#     )

#     type = django_filters.ChoiceFilter(choices=Enquiry.TYPES, widget=forms.Select(attrs={'class': 'form-control'}))
#     priority = django_filters.ChoiceFilter(
#         choices=Enquiry.PRIORITIES, widget=forms.Select(attrs={'class': 'form-control'}))

#     agency = django_filters.ModelChoiceFilter(
#         queryset=User.objects.all(),
#         name='agency',
#         method= 'filter_by_agent',
#         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
#     )


#     class Meta:
#         model = Enquiry
#         fields = ['code', 'type', 'status', 'min_amount', 'max_amount', 'contact', 'created_at', 'agency']

#     def filter_by_contact(self, queryset, name, value):
#         return queryset.filter(
#             Q(contact__name__icontains=value) | Q(contact__customer__name__icontains=value)
#         )

#     def order_by_field(self, queryset, name, value):
#         return queryset.order_by(value)

#     def filter_by_status(self, queryset, name, value):
#         return queryset.filter(
#             Q(status__name=value)
#         )
#     def filter_by_code(self, queryset, name, value):
#         return queryset.filter(
#             Q(customer__code__icontains=value)
#         )

#     def filter_by_agent(self, queryset, name, value):
#         return queryset.filter(
#             Q(customer__refered_by=value)
#         )

# class OfficeFilter(django_filters.FilterSet):
#     office = django_filters.ModelChoiceFilter(
#         queryset=Office.objects.all(),
#         method='filter_by_office',
#         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
#     )
#     min_amount = django_filters.NumberFilter(name="amount", lookup_expr='gte')
#     max_amount = django_filters.NumberFilter(name="amount", lookup_expr='lte')
#     contact = django_filters.CharFilter(name="contact__name")
#     type = django_filters.MultipleChoiceFilter(choices=Enquiry.TYPES)

#     class Meta:
#         model = Enquiry
#         fields = ['type', 'status', 'min_amount', 'max_amount', 'contact']




from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Students',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
            #  children=[
            #     item(
            #         'My Timesheets',
            #         'timesheet:list',
            #         in_menu=True,
            #         in_sitetree=True,
            #         access_loggedin=True,
            #         icon_class="calendar",
            #     ),
            # ]

        ),

        # item(
        #     'Task',
        #     'project:task:list project.id',
        #     in_menu=True,
        #     in_sitetree=True,
        #     access_loggedin=True,
        #     icon_class="clipboard",
        # )
    ]),
    # ... You can define more than one tree for your app.
)

from django.db import models
from address.models import Contact, Address
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting
BaseCodeSetting
from location.models import City
from enquiry.models import Enquiry
from product.models import Program
from crm.models import Campaign

# Create your models here.


class Student(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    def newCode():
        return StudentCodeSetting.new_code()
    code = models.CharField(max_length=20, default=newCode, unique=True)
    name = models.CharField(max_length=255)
    name_of_college = models.CharField(max_length=100, null=True, blank=True)
    year_of_passout = models.IntegerField(null=True, blank=True)
    photo = models.FileField(upload_to='stud/%Y/%m/%d', blank=True, null=True)
    location = models.ForeignKey(City, null=True, blank=True)
    enquiry = models.OneToOneField(Enquiry, null=True, blank=True, related_name='student')
    course = models.ForeignKey(Program, null=True, blank=True)
    gender = models.CharField(max_length=8, choices=SEX, blank=True, null=True)
    campaign = models.ForeignKey(Campaign, null=True, blank=True)

    def __str__(self):
        return (self.name)

    def save(self, *args, **kwargs):

        if self.code and self.created_at is None:
            StudentCodeSetting.incrementCountIndex()

        return super(Student, self).save(*args, **kwargs)


class AddressForStudent(Address):
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name="student_address")

    def __str__(self):
        return str(self.student)


class ContactForStudent(Contact):
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name="student_contact")

    def __str__(self):
        return str(self.student)


class StudentCodeSetting(BaseCodeSetting):
    pass

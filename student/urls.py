from django.conf.urls import include, url

# from employee import views, api
from student import views

app_name = "student"


urlpatterns = [
    url(r'^student/', include([
        url(r'^$', views.index, name="list"),
        # url(r'^create/$', views.create, name="create"),
        url(r'^create/(?P<pk>[^/]+)$', views.create, name="create"),
        url(r'^edit/(?P<pk>[^/]+)$', views.edit, name="edit"),
        url(r'^delete/(?P<pk>[^/]+)$', views.delete, name="delete"),
        url(r'^detail/(?P<pk>[^/]+)$', views.detail, name="detail"),

    ])),
]

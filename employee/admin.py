from django.contrib import admin
from .models import *
from guardian.admin import GuardedModelAdmin


class EmployeeAdmin(GuardedModelAdmin):
    pass


admin.site.register(Employee, EmployeeAdmin)
admin.site.register(AddressForEmployee)
admin.site.register(ContactForEmployee)
# admin.site.register(Targets)
# admin.site.register(Bonus)
# admin.site.register(DistanceTraveled)
# admin.site.register(Batterystatus)
# admin.site.register(IncentiveSlab)

# from rest_framework import serializers
# from django.contrib.auth.models import User

# from employee.models import Employee, Batterystatus
# from company.serializers import DesignationSerializer


# class UserSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = User
#         fields = ('first_name', 'username')


# class EmployeeSerializer(serializers.ModelSerializer):
#     user = UserSerializer(read_only=True)
#     primary_designation = DesignationSerializer()

#     class Meta:
#         model = Employee
#         fields = ('user', 'id', 'code', 'primary_designation')


# class BatterystatusSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Batterystatus
#         fields = '__all__'

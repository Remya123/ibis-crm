from autoslug import AutoSlugField
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from address.models import *
from company.models import *
from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator


class AddressForEmployee(Address):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_address")

    def __str__(self):
        return str(self.employee)


class ContactForEmployee(Contact):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_contact")

    def __str__(self):
        return str(self.employee)


class Employee(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    MARITALSTATUS = (
        ('Single', 'Single'),
        ('Married', 'Married'),
    )

    BLOODGROUP = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
    )
    code = models.CharField(max_length=255, unique=True)
    photo = models.FileField(upload_to='prof/%Y/%m/%d', blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=8, choices=SEX, blank=True, null=True)
    marital_status = models.CharField(max_length=10, choices=MARITALSTATUS, blank=True, null=True)
    blood_group = models.CharField(max_length=3, choices=BLOODGROUP, blank=True, null=True)
    languages_known = models.CharField(max_length=255, blank=True, null=True)
    birth_place = models.CharField(max_length=100, blank=True, null=True)
    cast = models.CharField(max_length=100, null=True, blank=True)
    religion = models.CharField(max_length=100, null=True, blank=True)
    citizenship = models.CharField(max_length=100, blank=True, null=True)
    status = models.BooleanField(default=True)

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)

    primary_designation = models.ForeignKey(
        Designation, on_delete=models.CASCADE)

    secondary_designation = models.ManyToManyField(
        Designation, related_name="employee_secondary_designation", blank=True)

    reporting_officer = models.ForeignKey(
        'employee.Employee', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        permissions = (
            ("view_employee_list", "Can view employee list"),
            ("view_employee_details", "Can view employee details"),
            ("can_access", "Can access Employee Section"),
            ("reporting_officier", "I am a reporting officier"),
            ("is_support", "Support")
        )

    def get_absolute_url(self):
        return reverse('employee:profile', args=[self.code])

    def __str__(self):
        return str(self.user.first_name) + " (" + str(self.code) + ")"


class EmployeeCode(ErpAbstractBaseModel):

    character_set = models.CharField(max_length=100)


class Targets(ErpAbstractBaseModel):
    amount = models.IntegerField(default=0, verbose_name="Target")
    employee = models.ForeignKey(Employee)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    incentive_amount = models.DecimalField(
        max_digits=200, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(0)])
    percentage = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    def __str__(self):
        return str(self.employee) + "-" + " from " + str(self.from_date) + " to " + str(self.to_date)

    # class Meta:
    #     unique_together = ("from_date", "to_date")

    # def unique_error_message(self, model_class, unique_check):
    #     print(model_class)
    #     import pdb; pdb.set_trace()
    #     if model_class == type(self) and unique_check == ("employee", "from_date", "to_date"):
    #         return 'My custom error message'
    #     else:
    #         return super(Targets, self).unique_error_message(model_class, unique_check)

# class Incentive(ErpAbstractBaseModel):
#     target = models.OneToOneField(Targets,related_name='target_incentive')
#     def __str__(self):
#         return str(self.target)


# class Batterystatus(ErpAbstractBaseModel):
#     employee = models.ForeignKey(Employee)
#     date_and_time = models.DateTimeField()
#     level = models.PositiveIntegerField()
#
#     def __str__(self):
#         return str(self.employee)


# class Bonus(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255, unique=True)
#     amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])
#
#     def __str__(self):
#         return str(self.name)


# class DistanceTraveled(models.Model):
#     distance = models.IntegerField(default=0, validators=[MinValueValidator(0)])
#     date = models.DateField()
#     employee = models.ForeignKey(Employee)
#
#     class Meta:
#         unique_together = ("date", "employee")
#
#     def __str__(self):
#         return str(self.employee)


class IncentiveSlab(ErpAbstractBaseModel):
    PARTNER = 'partner'
    COUNSELLOR = 'counselor'
    TYPES = (
        (PARTNER, "partner"),
        (COUNSELLOR, "counselor"),
    )

    start_value = models.IntegerField()
    end_value = models.IntegerField()
    employee_type = models.CharField(max_length=20, choices=TYPES, default=COUNSELLOR)
    amount = models.IntegerField()

    def __str__(self):
        return str(self.amount)


class EmployeePayout(ErpAbstractBaseModel):
    REQUESTED = 'requested'
    NOTREQUESTED = 'not requested'
    APPROVED = 'approved'
    STATUS = (
        (REQUESTED, "requested"),
        (NOTREQUESTED, "not requested"),
        (APPROVED, "approved"),
    )
    employee = models.ForeignKey(Employee, related_name="payout")
    request_date = models.DateField()
    pay_out_date = models.DateField(null=True, blank=True)
    approved_by = models.ForeignKey(Employee, related_name="payout_approved_by", null=True, blank=True)
    amount = models.DecimalField(max_digits=50, decimal_places=4)
    status = models.CharField(max_length=20, choices=STATUS, default=NOTREQUESTED)

    def __str__(self):
        return str(self.employee)

    class Meta:
        unique_together = ("employee", "request_date")

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F, Q, functions, Value as V
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from django.shortcuts import get_object_or_404, render
from guardian.shortcuts import get_objects_for_user
from location.models import *
from django.contrib.auth.models import Group
import datetime
from django.db.models import Sum
from django.contrib import messages
from erp_core.pagination import paginate
import datetime
from .models import *
from django.utils import timezone




@login_required
@permission_required('employee.view_employee_list', raise_exception=True)
def index(request):
    employees = get_objects_for_user(request.user, 'employee.view_employee_details', accept_global_perms=False)
    context = {
        'employees': employees
    }
    return render(request, "employee/index.html", context)


@login_required
def select2_list(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            Q(user__username__icontains=employee_name) | Q(user__first_name__icontains=employee_name)
        ).annotate(
            text=functions.Concat('user__username', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)


def select_employee(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            user__first_name__contains=employee_name
        ).annotate(
            text=functions.Concat('user__first_name', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        # print(employees)
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)


def select_designation(request):
    office_name = request.GET.get('office_name', False)
    if office_name:
        office = Office.objects.get(name=office_name)
        designations = Designation.objects.filter(office=office
                                                  ).annotate(
            text=('name')
        ).values('id', 'text')
        # print(employees)
        return JsonResponse(list(designations), safe=False)
    return JsonResponse(list(), safe=False)


def select_office(request):
    office = request.GET.get("office")

    offices = list(Office.objects.filter(name__contains=office).values('id', 'name').distinct())

    if request.is_ajax():
        return JsonResponse(offices, safe=False)


# @login_required
# @permission_required('employee.add_employee', raise_exception=True)
def create(request):
    form1 = UserForm(request.POST or None)
    form2 = EmployeeForm(request.POST or None, request.FILES or None)
    form3 = AddressForEmployeeForm(request.POST or None)
    form4 = ContactForEmployeeForm(request.POST or None)

    if request.method == "POST":

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()

            group1 = get_object_or_404(Group, name='employee')
            user.groups.add(group1)

            if employee.reporting_officer:
                #timesheet = TimeSheet.objects.update_or_create(employee=employee, approved_by=employee.reporting_officer)
                permission = Permission.objects.get(codename="reporting_officier")
                employee.reporting_officer.user.user_permissions.add(permission)
            if employee.primary_designation:
                for pr in employee.primary_designation.permissions.all():
                    employee.user.user_permissions.add(pr)
            return HttpResponseRedirect(reverse('employee:list'))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form_url': reverse_lazy('employee:create')

    }

    return render(request, 'employee/create.html', context)

#
# @login_required
# @permission_required('employee.change_employee', raise_exception=True)


def edit(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    reporting_officer = employee.reporting_officer
    photo = employee.photo

    if request.method == "POST":
        # print(request.POST)
        form1 = UserEditForm(request.POST, instance=employee.user)
        form2 = EmployeeForm(request.POST, request.FILES, instance=employee)
        form3 = AddressForEmployeeForm(request.POST, instance=employee.employee_address)
        form4 = ContactForEmployeeForm(request.POST, instance=employee.employee_contact)

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():

            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            if employee.photo == "":
                employee.photo = photo
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()
            if 'reporting_officer' in form2.changed_data:
                #timesheet = TimeSheet.objects.update_or_create(employee=employee, approved_by=employee.reporting_officer)
                permission = Permission.objects.get(codename="reporting_officier")
                employee.reporting_officer.user.user_permissions.add(permission)
                if reporting_officer:
                    remove_reporting_officer_permissions(reporting_officer.user, employee)
            return HttpResponseRedirect(reverse('employee:list'))

    else:

        form1 = UserEditForm(instance=employee.user)
        form2 = EmployeeForm(instance=employee)
        form3 = AddressForEmployeeForm(instance=employee.employee_address)
        form4 = ContactForEmployeeForm(instance=employee.employee_contact)

    context = {

        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'edit': "edit",
        'form_url': reverse_lazy('employee:edit', args=[employee.code])

    }

    return render(request, 'employee/create.html', context)


# @login_required
# @permission_required('employee.delete_employee', raise_exception=True)
def delete(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    # employee = Employee.objects.get(code=pk)
    employee.delete()
    return HttpResponseRedirect(reverse('employee:list'))


# @login_required
# # @permission_required('employee.view_employee_details', raise_exception=True)
def profile(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    return render(request, 'employee/profile.html', {'employee': employee})


@login_required
# @permission_required('employee.add_tagets', raise_exception=True)
def add_targets(request, pk):
    employee = get_object_or_404(Employee, id=pk) 

    form = TargetsForm(request.POST or None)
    if request.method == "POST":
        # already_exists = Targets.objects.filter(employee=request.user.employee, from_date__range=[request.POST['from_date'], request.POST[
        #                                        'to_date']], to_date__range=[request.POST['from_date'], request.POST['to_date']])
        from_date = datetime.datetime.strptime(request.POST['from_date'], "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(request.POST['to_date'], "%Y-%m-%d").date()
        
        
        already_exists_from_date = Targets.objects.filter(employee=request.user.employee, from_date__month=from_date.month, from_date__year=from_date.year)
        already_exists_to_date = Targets.objects.filter(employee=request.user.employee, to_date__month=to_date.month, to_date__year=to_date.year)
        if already_exists_from_date:
            messages.add_message(request, messages.INFO, 'Target for this month allready created')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        elif already_exists_to_date :
            messages.add_message(request, messages.INFO, 'Target for this month allready created')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:

            if form.is_valid():
                target = form.save(commit=False)
                target.employee = get_object_or_404(Employee, id=pk)
                target.save()
                return HttpResponseRedirect(reverse('employee:view_targets', args=[pk]))
        # else:
        #     messages.add_message(request, messages.INFO, 'Target for this month allready created')
        #     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    context = {
        "form": form,
        "pk": pk,
        "type": "add",
        'employee':employee,
    }
    return render(request, 'targets/addtarget.html', context)


def incentives(request, pk, from_date=None, to_date=None):
    today = datetime.date.today()
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        try:
            achievements = Targets.objects.get(from_date=from_date, to_date=to_date, employee_id=pk)

        except Targets.DoesNotExist:
            messages.add_message(request, messages.INFO, 'No Targets')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    else:
        try:
            achievements = Targets.objects.get(employee_id=pk, from_date__month=today.month,
                                               from_date__year=today.year, to_date__month=today.month, to_date__year=today.year)
        except Targets.DoesNotExist:
            messages.add_message(request, messages.INFO, 'No Targets')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    context = {
        'achievement': achievements,
        'today': today,
        'from_date': from_date,
        'to_date': to_date,
    }

    return render(request, "employee/view_incentive.html", context)


def incentive_list(request, from_date=None, to_date=None):
    today = datetime.date.today()
    if from_date and to_date:
        from_date = datetime.datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.datetime.strptime(to_date, "%Y-%m-%d").date()
        achievements = Targets.objects.filter(from_date=from_date, to_date=to_date)
    else:
        achievements = Targets.objects.filter(from_date__month=today.month, from_date__year=today.year,
                                              to_date__month=today.month, to_date__year=today.year)

    context = {
        'achievements': achievements,
        'today': today,
        'from_date': from_date,
        'to_date': to_date,
    }
    return render(request, "employee/incentive_list.html", context)


@login_required
def view_targets(request, pk):
    employee = get_object_or_404(Employee,pk=pk)

    targets = Targets.objects.filter(employee_id=pk).order_by("-created_at")
    targets, pagination = paginate(request, queryset=targets, per_page=10, default=True)
    context = {
        "targets": targets,
        "pk": pk,
        'pagination': pagination,
        'employee':employee
    }
    return render(request, 'targets/viewtargets.html', context)


@login_required
# @permission_required('employee.add_tagets', raise_exception=True)
def edit_targets(request, pk, target_pk):

    # target = get_object_or_404(Targets, id=target_pk)
    employee = get_object_or_404(Employee, id=pk) 
    target = Targets.objects.get(id=target_pk)
    form = TargetsForm(request.POST or None, instance=target)
    if request.method == "POST":
        form.save()
        return HttpResponseRedirect(reverse('employee:view_targets', args=[pk]))
    context = {
        "form": form,
        'form_url': reverse_lazy('employee:edit_targets', args=[pk, target_pk]),
        "pk": pk,
        "target_pk": target_pk,
        "type": "edit",
        'employee':employee,
    }
    return render(request, 'targets/addtarget.html', context)







def employee_incentives(request, pk):
    pass
    # credit = 0
    # pay_out = 0
    # enquiries = 0

    # employee = get_object_or_404(Employee, pk=pk)

    # if employee.primary_designation.name == "Counselor":
    #     incentives = IncentiveSlab.objects.filter(employee_type=IncentiveSlab.COUNSELLOR)
    #     # enquiries = Enquiry.objects.filter(successfully_closed_date__month=timezone.now().date().month,status=Status.objects.get(value="SUCCESSFULLY_CLOSED"), customer__counselor=request.user.employee).count()
    #     enquiries = Enquiry.objects.filter(successfully_closed_date__month=timezone.now().date().month,successfully_closed_date__year=timezone.now().date().year,status=Status.objects.get(value="SUCCESSFULLY_CLOSED"),customer__counselor=employee).count()
    #     for incentive in incentives:
    #         if enquiries >= incentive.start_value and enquiries <= incentive.end_value:
    #             credit = incentive.amount * enquiries
    # elif employee.primary_designation.name == "Partner":
    #     incentives = IncentiveSlab.objects.filter(employee_type=IncentiveSlab.PARTNER)
    #     enquiries = Enquiry.objects.filter(successfully_closed_date__month=timezone.now().date().month,successfully_closed_date__year=timezone.now().date().year, status=Status.objects.get(value="SUCCESSFULLY_CLOSED"), customer__created_by=employee).count()
    #     for incentive in incentives:
    #         if enquiries >= incentive.start_value and enquiries <= incentive.end_value:
    #             credit = incentive.amount * enquiries
    # if credit > 0 :
    #     if EmployeePayout.objects.filter(request_date__month=timezone.now().date().month,request_date__year=timezone.now().date().year).exists():
    #         pay_out = EmployeePayout.objects.get(request_date__month=timezone.now().date().month,request_date__year=timezone.now().date().year)

    # context = {
    #     'credit': credit,
    #     'date': timezone.now().date(),
    #     'employee':employee,
    #     'pay_out':pay_out,
    #     'enquiries':enquiries,

    # }
    return render(request, "employee/view_incentives.html", context)


def performance_report(request, pk):
    employee =get_object_or_404(Employee,pk=pk)

    date_for_check = datetime.date.today()
    targets = Targets.objects.filter(employee=employee)


    month_check = date_for_check.month
    year_check = date_for_check.year

    count = 0
    ince = 0
    data = []
    data2 = []
    data3 = []

    month_array = []
    first = date_for_check
    month_array.append(date_for_check.strftime("%B %Y"))

    while count < 4:
        first = first.replace(day=1)
        lastMonth = first - datetime.timedelta(days=1)
        month_array.append(lastMonth.strftime("%B %Y"))
        first = lastMonth
        count += 1


    count = 0
    incentive = []

    while count < 5:

        credit =0
        enquiries = Enquiry.objects.filter(successfully_closed_date__month=month_check,successfully_closed_date__year=year_check,status=Status.objects.get(value="SUCCESSFULLY_CLOSED"),customer__counselor=employee).count()
        open_enquiries = Enquiry.objects.filter(created_at__month=month_check,created_at__year=year_check,status=Status.objects.get(value="ENTRY"),customer__counselor=employee).count()
        targets = Targets.objects.filter(employee=employee,from_date__month=month_check,from_date__year=year_check)
        if targets:
            targets_set = Targets.objects.get(employee=employee,from_date__month=month_check,from_date__year=year_check).amount
        else:
            targets_set=0




        # if enquiries > 0:

        #     if employee.primary_designation.name == "Counselor":
        #         incentives = IncentiveSlab.objects.filter(employee_type=IncentiveSlab.COUNSELLOR)
        #         ince =1
        #     elif employee.primary_designation.name == "Partner":
        #         incentives = IncentiveSlab.objects.filter(employee_type=IncentiveSlab.PARTNER)
        #         ince = 1

        #     if ince == 1:
        #         for incentive in incentives:
        #             if enquiries >= incentive.start_value and enquiries <= incentive.end_value:
        #                 credit = incentive.amount * enquiries
                # incentive = credit
        # total_paid = installments.filter(status=True).aggregate(Sum('amount'))['amount__sum']
        # total_must_received = installments.aggregate(Sum('amount'))['amount__sum']
        # amount_due = installments.filter(status=False).aggregate(Sum('amount'))['amount__sum']

        # if not credit:
        #     incentive = 0

        data.append(enquiries)
        data2.append(open_enquiries)
        data3.append(targets_set)

        if month_check == 1:
            month_check = 12
            year_check = year_check - 1
        else:
            month_check = month_check - 1
        count += 1
    x = []
    
    x = zip(month_array, data,data2,data3)

    context = {
        'x': x,
    }
    return render(request, "employee/graph.html", context)

def request_for_payout(request, pk):
    pay_out=0
    employee = get_object_or_404(Employee,pk=pk)
    if employee.primary_designation.name == "Counselor":
        incentives = IncentiveSlab.objects.filter(employee_type=IncentiveSlab.COUNSELLOR)
        enquiries = Enquiry.objects.filter(successfully_closed_date__month=timezone.now(
        ).date().month, status=Status.objects.get(value="SUCCESSFULLY_CLOSED"), customer__counselor=request.user.employee).count()
        for incentive in incentives:
            if enquiries >= incentive.start_value and enquiries <= incentive.end_value:
                credit = incentive.amount * enquiries
    elif employee.primary_designation.name == "Partner":
        incentives = IncentiveSlab.objects.filter(employee_type=IncentiveSlab.PARTNER)
        enquiries = Enquiry.objects.filter(successfully_closed_date__month=timezone.now(
        ).date().month, status=Status.objects.get(value="SUCCESSFULLY_CLOSED"), customer__created_by=request.user.employee).count()
        for incentive in incentives:
            if enquiries >= incentive.start_value and enquiries <= incentive.end_value:
                credit = incentive.amount * enquiries
    # print (credit)
    if EmployeePayout.objects.filter(request_date__month=timezone.now().date().month,request_date__year=timezone.now().date().year).exists():
        # print ("hi")
        pay_out = EmployeePayout.objects.get(request_date__month=timezone.now().date().month,request_date__year=timezone.now().date().year)
        messages.add_message(request, messages.INFO, 'Your Payment is requested for payout')
        return HttpResponseRedirect(reverse('employee:employee_incentives',args=[employee.pk]))
    else:
        EmployeePayout.objects.create(employee=employee,request_date=timezone.now().date(),amount=credit,status=EmployeePayout.REQUESTED)
        messages.add_message(request, messages.INFO, 'Your Payment is requested for payout')
        return HttpResponseRedirect(reverse('employee:employee_incentives',args=[employee.pk]))


def all_incentives(request, pk):
    employee = get_object_or_404(Employee,pk=pk)
    payouts  = EmployeePayout.objects.filter(employee=employee)
    context ={
        'payouts':payouts,
        'employee':employee,
    }
    return render(request, "employee/all_incentives.html", context)

def approve_incentive(request, pk,incentive_pk):
    employee = get_object_or_404(Employee,pk=pk)
    payout  = EmployeePayout.objects.get(pk=incentive_pk)
    payout.approved_by=request.user.employee
    payout.status = EmployeePayout.APPROVED
    payout.save()
    return HttpResponseRedirect(reverse('employee:all_incentives',args=[employee.pk]))

def incentiveslab_list(request):
    slabs=IncentiveSlab.objects.all()
    context={
        "slabs":slabs,
    }
    return render(request, "employee/slab_list.html", context)

def add_slab(request):
    slab_form = IncentiveSlabForm(request.POST or None)
    if request.method=="POST":
        # print(request.POST.get('start_value'))
        # print(request.POST.get('end_value'))
        s = request.POST.get('start_value') 
        e = request.POST.get('end_value')

        if s < e:
            if slab_form.is_valid():
                slab_form.save()
                return HttpResponseRedirect(reverse('employee:slab_list'))

        else:
            messages.add_message(request, messages.ERROR, 'Start Value cannot be greater than End Value')

    context = {
        'slab_form':slab_form,
        'form_url':reverse_lazy('employee:add_slab'),
    }
    return render(request, "employee/edit_slab.html", context)


def edit_slab(request,pk):
    slab = get_object_or_404(IncentiveSlab,pk=pk)
    slab_form = IncentiveSlabForm(request.POST or None,instance=slab)
    if request.method=="POST":
        s = request.POST.get('start_value') 
        e = request.POST.get('end_value')
        if s < e:
            if slab_form.is_valid():
                slab_form.save()
                return HttpResponseRedirect(reverse('employee:slab_list'))
        else:
            messages.add_message(request, messages.ERROR, 'Start Value cannot be greater than End Value')

    context = {
        'slab_form':slab_form,
        'form_url':reverse_lazy('employee:edit_slab',args=[pk]),
        'edit':1,
    }
    return render(request, "employee/edit_slab.html", context)


def delete_slab(request,pk):
    slab = get_object_or_404(IncentiveSlab,pk=pk)
    slab.delete()
    return HttpResponseRedirect(reverse('employee:slab_list'))
    

def delete_targets(request,pk,target_pk):
    employee = get_object_or_404(Employee,pk=pk)
    target = get_object_or_404(Targets,pk=target_pk)
    target.delete()
    return HttpResponseRedirect(reverse('employee:view_targets',args=[employee.pk]))


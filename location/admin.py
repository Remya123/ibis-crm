from django.contrib import admin

from .models import *


# from nested_inline.admin import NestedStackedInline, NestedModelAdmin

class CityInline(admin.TabularInline):
    model = City
    extra = 0
    # list_display = ('name', 'city',)


class StateInline(admin.TabularInline):
    model = State
    extra = 0
    inlines = [CityInline]
    # list_display = ('name', 'country',)


class CountryAdmin(admin.ModelAdmin):
    inlines = [StateInline]


class StateAdmin(admin.ModelAdmin):
    inlines = [CityInline]
    exclude = ('country',)
    list_display = ('name', 'country',)


class OfficeInline(admin.TabularInline):
    model = Office
    extra = 0


class LocationAdmin(admin.ModelAdmin):
    inlines = [OfficeInline]


# Register your models here.
admin.site.register(Level)
admin.site.register(Zone)
admin.site.register(Country, CountryAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Location, LocationAdmin)

from django.apps import AppConfig


class ErpCoreConfig(AppConfig):
    name = 'erp_core'

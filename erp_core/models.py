import uuid
from django.core.validators import MinValueValidator

from django.db import models

# from autoslug import AutoSlugField
# Create your models here.
DISCOUNT_ABSOLUTE = "absolute"
DISCOUNT_PERCENTAGE = "percentage"
DISCOUNT_CHOICES = (
    (DISCOUNT_ABSOLUTE, "Rs"),
    (DISCOUNT_PERCENTAGE, "%"),

)


class UpdateMixin(object):
    def update(self, **kwargs):
        if self._state.adding:
            raise self.DoesNotExist
        for field, value in kwargs.items():
            setattr(self, field, value)
        self.save(update_fields=kwargs.keys())


class ErpAbstractBaseModel(models.Model, UpdateMixin):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        for field_name in ['title', 'name']:
            if hasattr(self, field_name):
                val = getattr(self, field_name, False)
                if val:
                    # print(field_name, val)
                    setattr(self, field_name, val.title())
        super(ErpAbstractBaseModel, self).save(*args, **kwargs)


class CustomQuerySet(models.QuerySet):

    def delete(self, force_delete=False, *args, **kwargs):
        # print("delete in ", self.__class__.__name__, " queryset")
        if force_delete:
            kwargs.pop('force_delete')
            return super(CustomQuerySet, self).delete(*args, **kwargs)
        for obj in self:
            obj.delete()


class BaseManager(models.Manager):
    def get_queryset(self):
        return super(BaseManager, self).get_queryset().filter(deleted=False)


class BaseModel(ErpAbstractBaseModel):

    objects = BaseManager.from_queryset(CustomQuerySet)()
    withDeleted = models.Manager()

    deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def isTrashed(self):
        return self.deleted is True

    def delete(self, force_delete=False, *args, **kwargs):
        # print("delete in ", self.__class__.__name__, "BaseModel")
        if force_delete:
            kwargs.pop('force_delete')
            return super(BaseModel, self).delete(*args, **kwargs)
        self.deleted = True
        self.save()


class BaseCodeSetting(ErpAbstractBaseModel):
    id = models.AutoField(primary_key=True)
    prefix = models.CharField(max_length=20, null=True, blank=True)
    count_index = models.PositiveIntegerField(default=1, validators=[MinValueValidator(0)])
    no_of_characters = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])

    class Meta:
        abstract = True

    def __str__(self):
        return self.generateCode()

    def save(self, *args, **kwargs):
        if self.pk is None and self.prefix:
            self.prefix = self.prefix.upper()

        return super(BaseCodeSetting, self).save(*args, **kwargs)

    @classmethod
    def new_code(cls):
        code = ""
        if cls.objects.exists():
            code_setting = cls.objects.last()
            code = code_setting.generateCode()
        return code

    def generateCode(self):
        code_prefix = self.prefix
        count = self.count_index
        no_of_chars = self.no_of_characters
        return str(code_prefix) + str(format(count, '0' + str(no_of_chars) + 'd'))

    @classmethod
    def incrementCountIndex(cls):
        if cls.objects.exists():
            code_setting = cls.objects.last()
            code_setting.count_index = models.F('count_index') + 1
            code_setting.save()


class ActivityType(ErpAbstractBaseModel):
    name = models.CharField(max_length=50)

    def __str__(self):
        return str(self.name)

# class PaymentType(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255)

#     def __str__(self):
#         return str(self.name)


# class PaymentTerm(ErpAbstractBaseModel):
#     term = models.CharField(max_length=10)
#     percentage_value = models.IntegerField()
#     type = models.ForeignKey(PaymentType, null=True, blank=True)

#     def __str__(self):
#         return str(self.term) + "-" + str(self.type) + "-" + str(self.percentage_value) + "%"

#     class Meta:
#         verbose_name_plural = "PaymentTerms"


# class Category(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255, unique=True)

#     def __str__(self):
#         return str(self.name)

#     class Meta:
#         verbose_name_plural = "Levels Of Course"
#         verbose_name = "Levels Of Courses"


# class AreaOfInterest(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255, unique=True)

#     def __str__(self):
#         return str(self.name)

# class Qualification(ErpAbstractBaseModel):
#     name = models.CharField(max_length=255)

#     def __str__(self):
#         return self.name


# class LanguageAssessmentTest(BaseModel):
#     name = models.CharField(max_length=255)

#     def __str__(self):
#         return self.name.upper()

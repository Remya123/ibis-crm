from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect, HttpResponse
from .models import ActivityType
from .forms import ActivityTypeForm
from django.core.urlresolvers import reverse_lazy, reverse

# Create your views here.
def activity_type_create(request):

    # supplier = get_object_or_404(Supplier, id=pk)

    activity_type_form = ActivityTypeForm(request.POST or None)

    if request.method == "POST":
        if activity_type_form.is_valid():

            activity = activity_type_form.save(commit=False)
            activity.save()
            HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    context = {
        # 'supplier': supplier,
        'activity_type_form': activity_type_form,
        'form_url': reverse_lazy('erp_core:activity_type_create'),
        'type': 'Add',
    }
    return render(request, "erp_core/activity_type_edit.html", context)